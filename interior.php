
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Block 9D | Jordan Hill</title>
</head>
<body>
<div class="wrapper">
<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$view = "";

if (isset($_GET['view'])) {
    $view = $_GET['view'];
}


$postid = $_REQUEST['id'];

?>
<link rel="stylesheet" href="css/loader.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="js/loader.class.js"></script>

<div id="screen-loader-exterior" style="position: absolute;top:0;left:0;width:100vw;height:100vh;background-color: #fff;z-index: 2000;">
    <div class="loader-outer">
      
        <div  id="loader-perc" class="loader-perc" data-pct="0" style="display:none;"><sup style="font-size:36px;">%</sup></div>
     
        <svg width="500" height="500">
        <path id="circular-gray-exterior" fill="none" stroke="#ebebeb" stroke-width="49" />
            <path id="circular-ring-exterior" fill="none" stroke="#87674f" stroke-width="39" />
            <path id="circular-loader-exterior" fill="none" stroke="#203e35" stroke-width="39" />
        
        </svg>
   </div><!-- .loader-outer -->
     
</div><!-- #screen-loader -->

<link rel="stylesheet" href="css/interior.css" type="text/css" rel="stylesheet"/>
<!-- <link rel="stylesheet" href="custom-int.css" type="text/css" rel="stylesheet"/>  -->
<link rel="stylesheet" href="css/style.css" type="text/css" rel="stylesheet"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js"></script>



<script type="text/javascript" src="js/three.min.js"></script>
<script type='text/javascript' src='js/jquery.imgpreload.js'></script>

<style>
.active_info{
        background-image: url("images/info_active.png"); 

    }
</style>



<script type="text/javascript">
    // var site_path= "http://192.168.0.30/360/tulloch-homes/";
window.selfnav_id =  <?=$postid?>;
//initiate as false
var isMobile = false;
// device detection
    // if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    //         || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))
        isMobile = true;

    

$(document).ready(function (e) {

        if (!isMobile) {
            $("#gyro").hide();
           // passVariable('false');
        }
        else
        {
           // console.log('mobile');
          //  passVariable('true');
           //window.location = "https://tulloch-homes-homeselector.com/360/mobile-interior.php?id="+<?=$postid?>;
           //window.location = "http://192.168.0.30/360/tulloch-homes/mobile-interior.php?id="+<?=$postid?>;
        }

 var degree = 0;
document.getElementById("circular-gray-exterior").setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, 359.99));
document.getElementById("circular-ring-exterior").setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, 359.99));
$("#loader-perc").show();
      
     console.log("Browser Online Status: " + navigator.onLine);
    var online_status = navigator.onLine;
       if(online_status == true)
        {
            console.log('online');
            window.selfnav_id =  <?=$postid?>;
               $.ajax({
                    url: 'https://cala.thehomeselector.co.uk/wp/oak-phase/wp-json/api/v1/interior-360-data',
                    
                    method: 'post',
                    
                    data: { selfnav_id: <?=$postid?> } ,
                    
                    success: function( data, textStatus, jQxhr ){
                        //console.log(JSON.stringify(data));
                        init_exterior(data);
                        localStorage.setItem('virtual_visit_data', JSON.stringify(data));
                        //cache_images(data);
                            
                        window.data = data;                           
                        var selfnav =  data.selfnav;
                        window.house_type = selfnav.title;
                        $(".ht-name").append(selfnav.title);
                        $(".backButton").append(selfnav.title);
                       angular.element($("#floorplan-container")).scope().updateData(window.data);
                       angular.element($("#floorplan-container")).scope().$apply();
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                }); // end ajax call
        }
        else
        {
            console.log('offline');
            var response = JSON.parse(localStorage.getItem('virtual_visit_data'));
            console.log(response);
            //init_exterior(response);
            $("#screen-loader-exterior").hide();
            var selfnav =  response.selfnav;
            $(".ht-name").append(selfnav.title);
            $(".backButton").append(selfnav.title);
            window.data= response;
            LoadImages();
           
            setTimeout(function() {
                angular.element($("#floorplan-container")).scope().updateData(window.data);
                angular.element($("#floorplan-container")).scope().$apply();
            }, 100);
        }



    
                          
    });//document ready close
    function searchViewIndex(view) {
        view = parseInt(view);
        for (var i = 0; i < data.scenes.length; i++) {
            var number = parseInt(data.scenes[i].scene_number)
            if (number == view) {
                return i;
            }
        }
    } // end funcction searchViewIndex
    function init_exterior(data)
    {
                    var j;
                    var k;
                    var images = [];
                    var len = data.scenes.length;
                    var self_fp = data.selfnav.floorplans;
                    //console.log(self_fp);
                    for (j = 0; j < len; j++) {
                        var scene_img = data.scenes[j].scene_image;
                        if(scene_img != "" && scene_img != null){
                            images.push(data.scenes[j].scene_image);
                        }

                    }
                    for (j = 0; j < len; j++) {
                        var scene_image_wof = data.scenes[j].scene_image_wof;
                        if(scene_image_wof != "" && scene_image_wof != null){
                             images.push(data.scenes[j].scene_image_wof);
                        }

                    }
                    for (j = 0; j < len; j++) {
                        //images.push(data.scenes[j].scene_desktop_image_dim);

                    }

                     for(var i=0; i<self_fp.length; i++){
                        var floorplan_image = self_fp[i].floorplan_image
                            if(floorplan_image != "" && floorplan_image != null){
                                images.push(floorplan_image);                   
                            }

                        var floorplan_image_wof = self_fp[i].floorplan_image_wof;
                            if(floorplan_image_wof != "" && floorplan_image_wof != null){
                                    images.push(floorplan_image_wof);                   
                            }

                    }

                     
                      LoadImages();
                    var no_images_loaded = 0;
                    var no_of_images = images.length;
    
         //console.log(images);

                      $.imgpreload(images, {
                            each:function(){
                                no_images_loaded++;
                               // console.log(no_images_loaded);
                                var percentage = (no_images_loaded / no_of_images) * 100;
                                percentage = Math.round(percentage);
                                  
                                $('#loader-perc').attr('data-pct',percentage);  
                                var degree = (no_images_loaded / no_of_images) * 360;
                                var pathElement = document.getElementById("circular-loader-exterior")
                                if (!pathElement) 
                                {
                                    return; // don't do the rest
                                }
                                pathElement.setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, degree));   
                            },
                            all:function ()
                        {
                            console.log("hide");
                          // $('#overlayLoader').hide();
                           $("#screen-loader-exterior").hide();
                           <?php
                            if ($view == "") {
                                ?>
                                loadScene(data.scenes[0].scene_id, true);
                                
                                
                            <?php } else { ?>
                                var viewindex = searchViewIndex(<?= $view ?>);
                                loadScene(data.scenes[viewindex].scene_id, true);
                                
                            <? } ?>
                        }
                       
                    });
                    

                    /*$.imgpreload(images,
                    {

                         each:function(){
                            no_images_loaded++;
                        
                            var percentage = (no_images_loaded / no_of_images) * 100;
                            percentage = Math.round(percentage);
                        
                            $('#loader-perc').attr('data-pct',percentage);  

                            var degree = (no_images_loaded / no_of_images) * 360;
                            var pathElement = document.getElementById("circular-loader-exterior")
                            if (!pathElement) 
                            {
                                return; // don't do the rest
                            }
                            pathElement.setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, degree));
                            },
                         all:function ()
                        {
                           
                                $("#screen-loader-exterior").hide();
                                <?php
                            if ($view == "") {
                                ?>
                                loadScene(data.scenes[0].scene_id, true);
                                
                            <?php } else { ?>
                                var viewindex = searchViewIndex(<?= $view ?>);
                                loadScene(data.scenes[viewindex].scene_id, true);
                            <? } ?>
                          
                                
                        }
         });*/
    }


    function passVariable(value){
        var x = value;
        console.log("x  = "+ x);
    if(x == 'true')
    {
            var url = document.location.href + '&x=' + x;
            var exists = document.location.href.indexOf('&x=');
            if(exists < 0){
                  // redirect passing variable
                  window.location = url;
            }
            
    }else{
        var url = window.location.href;
        url = url.split('&')[0];
        var exists = document.location.href.indexOf('&x=');
            if(exists > 0){
                  // redirect passing variable
                  window.location = url;
            }
       
    }
        
  }
</script>


<?php
  //  $isMobile = "<script>document.write(isMobile);</script>";
  if(isset($_REQUEST['ui']))
   $isMobile = true;
  else{
    $isMobile = false;
  }
    // if($postid == 1 || $postid == 2){
    //      $displayTags = "display:none";
    // }else{
    //      $displayTags = "display:block";
    // }

    if($isMobile){
        $displayTags = "display:none";
        $displaydesk = "display:block";
    }else{
        $displayTags = "display:block";
        $displaydesk = "display:none";
    }
?>

<div id="labels" style="position:absolute;left:0px;top:0px;"></div>
<div id="info-popup" class= "info" style="display:none;"></div><!-- #labels -->

        <!-- <script type="text/javascript">

            var fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
            var fullscreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled;
            function launchIntoFullscreen(element) {
                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }

                var lock = window.navigator.requestWakeLock('screen');
            }

// Launch fullscreen for browsers that support it!
            addEventListener("touchend", function () {
                launchIntoFullscreen(document.documentElement); // the whole page
            });
        </script> -->




</div>
</div>

<!-- <div id="overlay" style="position: absolute; top: 0px; left: 0px; width: 100vw; height: 100vh; background-color: rgb(51, 51, 51); z-index: 1000;display:none">
    <div class="overlay_inner">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
        <h4>  Please wait while your virtual tour loads.... </h4>
    </div>
</div>  -->

<ul style="position:absolute;top:10px;left:10px;">
<?php

//Temporary code 
// Remove once viewpoint map implemented
for($c=0; $c<2; $c++){
    ?>
    <!-- <li style="width:60px;display:inline;cursor:pointer;" onclick="loadScene(data.scenes[<?=$c?>].scene_no, true);">VIEW <?=$c+1?> | </li> -->

<?php
}

// end of termporary code
?>

</ul>
<div class="development-logo" style="<?=$displayTags?>">  
    <img class="img-responsive" src="./images/jordan-hill-logo.png"> 
     <div class="ht-name"></div>
</div>

<!-- <div class="header-box" id="dimension-on-off" style="display:block;">
    <div class="label-dimension" style="display:block;" data-dimen="dimension-on" id="dimension-on" onclick="dimensionOnOff(this)">
    <img src="./images/Dim-on.png">
    </div> .dimension 

    <div class="label-dimension" style="" data-dimen="dimension-off" id="dimension-off" onclick="dimensionOnOff(this)">

    <img src="./images/Dim-off.png">
    </div>
</div> -->

 <div id="roomname" class="tag" style="<?=$displayTags?>"></div>



 <!-- Buttons navigation Scenes-->

 <img id="nav-previous" class="nav-handles" src="./images/previous.png" onclick="fnPreviousScene()">
 <img id="nav-next" class="nav-handles" src="./images/next.png" onclick="fnNextScene()" >

 <!-- Buttons navigation Scenes-->

 <!-- .Floorplans Container starts -->

 <div id="floorplan-container" ng-app="appKey" ng-controller="ctrlKey" style="<?=$displayTags?>">


  
 <div class="floorplan-inner" >
 <div class="camera" id="people" style="display:none;z-index:101;position:absolute;">
        <div style="position:relative;">
            <img alt="0"  src="./images/camera.png" style="position:absolute;top:-41px;left:-41px;pointer-events: none;"/>
            
        </div>
    </div><!--#people -->
   <div class="floor"  ng-repeat="floorplan in floorplans" ng-show="floorplan.floor == floorid" id="floorid_{{floorplan.floor}}" >

 

   <div ng-repeat="tag in floorplan.tags" data-floor="{{floorplan.floor}}" class="btn_click btn-active-{{tag.scene_id}}" id= "{{tag.scene_id}}" title="{{tag.scene_label}}" 
    style="background-image:url(./images/icon-location-scene.png); left:{{tag.coord_x}}px;top:{{tag.coord_y}}px;"
    target="{{tag.scene_id}}" onclick="loadSceneFromFloorplan(this)"  ></div>

               
      <img id="floorplan_{{floorplan.floor}}" class="floor-img floor-wf"  ng-src=" {{floorplan.floorplan_image}}" style="display: block;" />
      <img id="floorplan_{{floorplan.floor}}" class="floor-img floor-wof"  ng-src="{{floorplan.floorplan_image_wof}}" style="display: none;"/>

    </div>
 </div>

 <div class="floor-buttons" >
     <div class="div-centering">
         
         <!-- <div class="btn-floor plan_{{fl.floor}}  floor-clicks"   id="button"  ng-repeat="fl in floorplans" ng-click="togglefloorplans($index)"  ng-class="{active: $index== floorid}" target="{{arrfloor[fl.floor]}}" ng-style="{ 'width' : 'calc(209px/{{floorplans.length}})'}">{{arrfloor[fl.floor]}}</div> -->
         <!-- <div class="btn-floor plan_{{fl.floor}}  floor-clicks"   id="button"  ng-repeat="fl in floorplans" ng-click="togglefloorplans($index)"  ng-class="{active: $index== floorid}" target="{{arrfloor[fl.floor]}}">{{arrfloor[fl.floor]}}</div> -->

         </div>
         
     </div>

</div>
<?php
//echo $_REQUEST['ui'];
if(isset($_REQUEST['ui'])){
?>
    <style rel="stylesheet">

        /*#floorplan-container{
            transform: scale(0.6) !important;
            bottom: -84px;
        }   
        .development-logo img

        {
            width:100px;
        }
        .ht-name
        {
            font-size: 20px;
            width: 100px;
            margin-left: 0px;
            text-align: center;
        }
        .tag
        {
        bottom:26px;
        }*/

        #floorplan-container{
                transform: scale(0.6) !important;
                top: 78px;
                left: 12px; 
                /*bottom: -84px;*/
                bottom: -70px;
                right: 25px;
            }   
            .development-logo img

            {
                width:100px;
            }
            .ht-name
            {
                font-size: 20px;
                width: 100px;
                margin-left: 0px;
                text-align: center;
            }
            .tag
            {
            bottom:26px;
            }
            .top .round-img
            {
                display:block;
                width: 26px;
                left: -25px;
                top: -6px;
            } 
            .bottom .round-img
            {
                display:block;
                width: 26px;
                left: -25px;
                top: -6px;
            } 
            .left .round-img
            {
                display:block;
                width: 26px;
                left: -25px;
                top: -6px;
            }
            .right .round-img
            {
                display:block;
                width: 26px;
                right: -5px;
                top: -6px;
            }
            .flat-img
            {
                display:none;
            }
            #labels .hotspot
            {
                border-radius:14px;
                min-width: 110px !important; 
                font-size: 16px;
                color: #000;
                padding: 7px 7px 8px 27px;
                text-align:left;
                
            }
            #labels .hotspot span
            {
                margin-left:10px; 
                margin-right:10px;
            }
            #labels .hotspot.right
            {
                padding: 7px 7px 8px 14px;
            }

            #labels .hotspot.right span
            {   
                margin-right: 29px;
            }
            .label-name
            {
             width:130px;
             bottom: 25px;
             right: 0px;
             /*width:70px;
             right:200px;
             bottom: 55px;*/
             height: 0px;
             position: fixed;
            }
          
            .fur-mob
            {
                width:15.4px;
                margin-top: -11px;
            }
            .fur-label
            {
                font-size:10px;
                color:#fff;
                padding:0px 7px;
                letter-spacing: 1.5px;
                text-transform: uppercase;
            }
            #gyro {
                cursor: pointer;
                left: 10px;
                top: 46px;
              
            }

    </style>
    <?php
}
if(isset($_REQUEST['web'])){
?>
<style rel="stylesheet">

#floorplan-container{
    transform: scale(0.6) !important;
    /* top: 78px;
    left: 12px; */
    /*bottom: -84px;*/
    bottom: -70px;
    right: 25px;
}   
.development-logo img

{
    width:100px;
}
.ht-name
{
    font-size: 20px;
    width: 100px;
    margin-left: 0px;
    text-align: center;
}
.tag
{
bottom:26px;
}
.top .round-img
{
    display:block;
    width: 26px;
    left: -25px;
    top: -6px;
} 
.bottom .round-img
{
    display:block;
    width: 26px;
    left: -25px;
    top: -6px;
} 
.left .round-img
{
    display:block;
    width: 26px;
    left: -25px;
    top: -6px;
}
.right .round-img
{
    display:block;
    width: 26px;
    right: -5px;
    top: -6px;
}
.flat-img
{
    display:none;
}
#labels .hotspot
{
    border-radius:14px;
    /* min-width: 110px !important; */
    font-size: 16px;
    color: #000;
    padding: 7px 7px 8px 27px;
    text-align:left;
    
}
#labels .hotspot span
{
    /* margin-left:10px; */
    margin-right:10px;
}
#labels .hotspot.right
{
    padding: 7px 7px 8px 14px;
}

#labels .hotspot.right span
{   
    margin-right: 29px;
}
.label-name
{
 /*width:130px;
 bottom: 25px;
 right: 0px;*/
 width:70px;
 right:200px;
 bottom: 75px;
 height: 0px;
 position: fixed;
}
.label-name img{
    width:100%;
}
.fur-mob
{
    width:15.4px;
    margin-top: -11px;
}
.fur-label
{
    font-size:10px;
    color:#fff;
    padding:0px 7px;
    letter-spacing: 1.5px;
    text-transform: uppercase;
}
#gyro{
    cursor: pointer;
    left: 10px;
    top: 46px;
  
}
@media screen and (min-width: 1025px) and (max-width: 1280px) {
        .label-name{
                left: 15px !important;
                bottom: 73px !important;
        }
        #floorplan-container{
                bottom: -90px !important;
                right:10px !important;
        }

}

</style>
    <?php
}
?>  
 <!-- .Floorplans Container ends -->

 <div class="header-box" style="<?=$displayTags?>">   

           <div class="label-name furniture-on" data-fur="furniture-on" id="furniture-on" onclick="switchFurniture()" style="display: none;">
                  <img src="./images/fur-on.png">
           </div>
           <div class="label-name furniture-off" data-fur="furniture-off" id="furniture-off" onclick="switchFurniture()" style="display: block;">
           <img src="./images/fur-off.png">
           </div>   

 </div>       <!-- header-box -->

 <div class="header-box" style="<?=$displaydesk?>">   

           <div class="label-name furniture-on" data-fur="furniture-on" id="" onclick="switchFurniture()" style="display: none;">
                  <div style="float:left"><img class="fur-mob" src="./images/furniture.png"></div><div class="fur-label" style="float:left">Furniture On</div>
           </div>
           <div class="label-name furniture-off" data-fur="furniture-off" id="" onclick="switchFurniture()" style="display: block;">
                 <div style="float:left"><img class="fur-mob" src="./images/furniture.png"></div><div class="fur-label" style="float:left">Furniture Off</div>
           </div>   

 </div>       <!-- header-box -->

 <!-- <div class="header-box">   

<div class="label-info" data-info="info-on" id="info-on" onclick="switchInfo()" style="display: none;">
       <img src="./images/info-on.png">
</div>
<div class="label-info" data-info="info-off" id="info-off" onclick="switchInfo()" style="display: block;">
<img src="./images/info.png">
</div>   

</div>     header-box -->

<!--<div class="dimen-table-outer" style="display:block;">
   <div id="dim-room-name" class="head">Lounge </div>
   <table class="dimension-table" cellspacing="0" border="0">
        <tbody>
            <tr>
                <th></th>
                <th>Imperial</th>
                <th>Metric</th>
            </tr>                           
            <tr>
                <td style="width:38%">Width</td>
              
                <td style="width:30%" id="width-imperial" class="center-text">0'0"</td>
                <td style="width:30%" id="width-metric" class="center-text">00 </td>
            </tr>     
            <tr>
                <td>Length</td>
                <td id="length-imperial" class="center-text">0'0"</td>
                <td id="length-metric" class="center-text">00 </td>
            </tr>    
            <tr>
                <td>Height</td>
                <td id="height-imperial" class="center-text">0'0"</td>
                <td id="height-metric" class="center-text">00 </td>
            </tr>
             <tr class="total">
                <td>Total Area :</td>
                <td id="area-imperial" class="center-text">00 sqft</td>
                <td id="area-metric" class="center-text">00 sqm</td>
            </tr>                	 
        </tbody>
   </table>
  
</div>--><!-- .dimen-table-outer -->

<div class="gyro-display">
    <img id="gyro" src="images/GSensor_Inactive.png" onclick="gyroOnOff();"/>  
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.7/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.7/angular-sanitize.js"></script>
<script src="js/interior.js" type="text/javascript"></script>
<script type="text/javascript">

console.log("Angular loaded");
var app = angular.module('appKey', ['ngSanitize']);

 app.controller('ctrlKey', function ($scope, $http) {
   // console.log("Browser Online Status: " + navigator.onLine);
    $scope.data = " ";
    $scope.updateData = function(data){
        $scope.data = data;
      // console.log($scope.data);
      //init_selector(data);
        $scope.selfnav = $scope.data.selfnav;
        $scope.floorplans = $scope.selfnav.floorplans;
        $scope.floorid =  $scope.floorplans[0].floor;
       $scope.arrfloor = ['Ground','First','Second','Third','Fourth'];
        //console.log("asd" + $scope.floorplans);
    }

    //  function init_selector(data){
        
    //      LoadImages();

    //  }
    
       $scope.togglefloorplans = function(element) {
                
               
                //$('.btn-floor').removeClass('active');
               // $('.plan_'+element).addClass('active');
               
                if(window.activeFloor == 'plan_'+element){
                    
                    $('#people').css('display','block');
                }else{
                    $('#people').css('display','none');
                }

                $scope.floorid = element;
             
            }
      

}); 
// app.config(['$compileProvider', function ($compileProvider) {
//   $compileProvider.debugInfoEnabled(false);
// }]);
jQuery(document).ready(function($) 
{
setTimeout(function(){
    // angular.element(document.body).scope().updateData(window.data);
    // angular.element(document.body).scope().$apply();
    // angular.element($("#floorplan-container")).scope().updateData(window.data);
    // angular.element($("#floorplan-container")).scope().$apply();
          loadScene(data.scenes[0].scene_id, true);

}, 2000);
});
</script>

<script>
    function dimensionOnOff(ele)
                   {
                   // console.log(ele);
                    var dimension = $(ele).attr('data-dimen');
                    if (dimension != 'dimension-on') {
                        $('#dimension-off').hide();
                        $('#dimension-on').show();
                       // $(".dimen-table-outer").hide();
                        sphereDim.visible = false;
                      } else {
                          $('#dimension-on').hide();
                          $('#dimension-off').show();
                        //  $(".dimen-table-outer").show();
                          sphereDim.visible = true;
                      }
                   
                   }

    function exitTheHouse()
    {
        if(history.length > 1)
        {
            window.history.back();
        }
       else{
           window.location = "https://www.tulloch-homes-homeselector.com/360/?id="+<?=$postid?>; 
       }
           
    }              
    </script>
    <!-- <div class="back-logo" onclick="exitTheHouse();">  
       <div class="exit">Exit the </div> <div class="backButton"></div> <div class="house">  House</div>
    </div> -->
</div><!-- End wrapper -->
</body>
</html>