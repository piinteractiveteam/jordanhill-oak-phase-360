    window.ajaxurl= 'https://cala.thehomeselector.co.uk/wp/oak-phase/wp-admin/admin-ajax.php'; // server url

     //window.environment = 'development';
    window.environment = 'production';

    var furniture = true;
    var info = false;

    Math.degrees = function (radians) {
        return radians * 180 / Math.PI;
    };
    var currentsceneno = 0;

    function fnNextScene() {  

        currentsceneno++;
        if (currentsceneno >= data.scenes.length) {
            currentsceneno = 0;
        }
        //    
        // var sceneInfo = searchSceneInfo(data.scenes[currentsceneno].scene_id);
        // console.log(sceneInfo.floor);  
    //     angular.element($("#floorplan-container")).scope().togglefloorplans(sceneInfo.floor);
    //    angular.element($("#floorplan-container")).scope().$apply();

    window.room_label_clicked = false;  
        window.floorplan_label_clicked = false;
        var vvdata = {"action": "vv_activity", "page":"interior", "vv_action":"next_arrow_click", "house_type":window.house_type ,"room_name":data.scenes[currentsceneno].scene_label, "vv_value":"", "selfnav_id":window.selfnav_id, "scene_id":data.scenes[currentsceneno].scene_id,"environment":window.environment};
    
                    $.ajax({url: window.ajaxurl, 
                         method:"POST", 
                         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                         data:$.param(
                             vvdata
                         )
                     });

        loadScene(data.scenes[currentsceneno].scene_id, true);

    }

function fnPreviousScene() {
        currentsceneno--;
        if (currentsceneno < 0) {
            currentsceneno = data.scenes.length - 1;
        }
        window.room_label_clicked = false;  
       window.floorplan_label_clicked = false;
        var vvdata = {"action": "vv_activity", "page":"interior", "vv_action":"previous_arrow_click", "house_type":window.house_type ,"room_name":data.scenes[currentsceneno].scene_label, "vv_value":"", "selfnav_id":window.selfnav_id, "scene_id":data.scenes[currentsceneno].scene_id, "environment": window.environment};

                $.ajax({url: window.ajaxurl, 
                      method:"POST", 
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                      data:$.param(
                          vvdata
                      )
                  });

        loadScene(data.scenes[currentsceneno].scene_id, true);

    }

    
    var data;
    var scene = new THREE.Scene();
    var sphereshift = 0.001;
    var yrotation = 0;
    var verticalshift = 0;
    var verticalmanualshift = 0;
    var gyro = false;
    var fov = 90;
    var camera = new THREE.PerspectiveCamera(fov, window.innerWidth / window.innerHeight, 0.1, 1000);
    var mouse = new THREE.Vector2(), INTERSECTED, intersects;
    var raycaster;
    var spOpacity = 1;
    var frustum = new THREE.Frustum();
    var cameraViewProjectionMatrix = new THREE.Matrix4();
    var SCREEN_WIDTH = window.innerWidth;
	var SCREEN_HEIGHT = window.innerHeight;
	var aspect = SCREEN_WIDTH / SCREEN_HEIGHT;

    var sceneMirroring = -1; //default -1 and for mirror 1


    var light = new THREE.DirectionalLight(0xffffff, 1);
    light.position.set(1, 1, 1).normalize();
    scene.add(light);
    raycaster = new THREE.Raycaster();
    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    $(document).ready(function () {
     document.body.appendChild(renderer.domElement);
        var canvass = document.getElementsByTagName('canvas');
        var canvas = canvass[0];
        var context = canvas.getContext('2d');
//        // codes...

    });



    var geometry = new THREE.SphereGeometry(5, 128, 128);
    var material = new THREE.MeshBasicMaterial({color: 0xffffff});
    var sphere = new THREE.Mesh(geometry, material);
    sphere.scale.x = sceneMirroring;
    scene.add(sphere);

    var geometryOpacity = new THREE.SphereGeometry(4.8, 128, 128);
    var materialOpacity = new THREE.MeshBasicMaterial({color: 0xffffff, opacity: spOpacity, transparent: true});
    var sphereOpacity = new THREE.Mesh(geometryOpacity, materialOpacity);
    sphereOpacity.scale.x = sceneMirroring;
    scene.add(sphereOpacity);

    var geometryDim = new THREE.SphereGeometry(4.6, 128, 128);
    var textureDim = new THREE.TextureLoader().load( "images/alpha.jpg" );
    textureDim.anisotropy = 16;
    textureDim.minFilter = THREE.LinearFilter;
    var materialDim = new THREE.MeshBasicMaterial({color: new THREE.Color( 0x0F7140 ), alphaMap:textureDim, side: THREE.DoubleSide, transparent: true});
    var sphereDim = new THREE.Mesh(geometryDim, materialDim);
    sphereDim.scale.x = sceneMirroring;
    sphereDim.visible = false;
    scene.add(sphereDim);

    

    camera.position.z = 0;
    camera.rotation.x = 0;
    var render = function () {

        requestAnimationFrame(render);
        raycaster.setFromCamera(mouse, camera);
        intersects = raycaster.intersectObjects(sphere.children);

        var spchild = sphere.children;
        for (var a = 0; a < spchild.length; a++) {

            if (spchild[a].type == 'plane') {
                showLabelOnScreen(spchild[a], camera);
            }

            if (spchild[a].type == 'group') {
                showPolygonsOnScreen(spchild[a], camera);
            }

        }

        if (gyro) {
            sphereDim.rotation.y = sphere.rotation.y = yrotation.toFixed(2);
            camera.rotation.x = verticalshift.toFixed(2);
        } else {
            //sphere.rotation.y += sphereshift;
            //sphere.rotation.y = 0;
            //camera.rotation.x = verticalmanualshift.toFixed(2);
        }

//console.log(sphere.rotation.y);
var rotation_val;
if(window.selfnav_id ==1)
{
    rotation_val = 90;

}else{
    rotation_val = -90;
    }

//console.log(rotation_val);
        var cameraRotation = Math.degrees(sphere.rotation.y) + (rotation_val);
        $(".camera").css("transform", "rotate(" + cameraRotation + "deg)");



        if (spOpacity > 0) {
            spOpacity -= 0.1; /* To manage Opacity Speed*/
            sphereOpacity.material.transparent = true;
            sphereOpacity.material.opacity = spOpacity;
        }

       // console.log(sphere.rotation.y);

        renderer.render(scene, camera);
    };
    render();


    function updateCameraProjection(){
                SCREEN_WIDTH = window.innerWidth;
				SCREEN_HEIGHT = window.innerHeight;
				aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
				renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
				camera.aspect = aspect;
                camera.updateProjectionMatrix();
                camera.fov = fov;
    }

    function showPolygonsOnScreen(group, camera) {
        var points = group.children;
        var coords = new Array();
        var visibles = new Array();
        for (var i = 0; i < points.length; i++) {
            var obj = points[i];
            var vector = toScreenPosition(obj, camera);
            coords.push(Math.round(vector.x));
            coords.push(Math.round(vector.y));
            visibles.push(toScreenVisible(obj, camera));
        }

        var coordsStr = coords.toString();

        $("#poly_" + group.hotspot_id).attr("points", coordsStr);

        var visible = isGroupVisible(visibles);

        if (visible) {
            $("#poly_" + group.hotspot_id).show();
        } else {
            $("#poly_" + group.hotspot_id).hide();
        }

    }

    function isGroupVisible(visibles) {
        for (var i = 0; i < visibles.length; i++) {
            if (visibles[i]) {
                return true;
            }
        }

        return false;
    }

    function showLabelOnScreen(obj, camera){
                var vector = toScreenPosition(obj, camera);
                var visible = toScreenVisible(obj, camera);
                
                if(visible){
                    $("#"+obj.hotspot_id).show();
                }else{
                    $("#"+obj.hotspot_id).hide();
                }

                $("#"+obj.hotspot_id)
                .css('left', (vector.x - $("#"+obj.hotspot_id).width() / 2) + 'px')
                .css('top', (vector.y - $("#"+obj.hotspot_id).height() / 2) + 'px');
            }


    function toScreenVisible(obj, camera) {
        // every time the camera or objects change position (or every frame)

        camera.updateMatrixWorld(); // make sure the camera matrix is updated
        camera.matrixWorldInverse.getInverse(camera.matrixWorld);
        cameraViewProjectionMatrix.multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse);
        frustum.setFromMatrix(cameraViewProjectionMatrix);

        // frustum is now ready to check all the objects you need                
        var visible = frustum.intersectsObject(obj);


        return visible;
    }

    function toScreenPosition(obj, camera) {


        var vector = new THREE.Vector3();

        var widthHalf = 0.5 * renderer.context.canvas.width;
        var heightHalf = 0.5 * renderer.context.canvas.height;

        obj.updateMatrixWorld();

        vector.setFromMatrixPosition(obj.matrixWorld);
        vector.project(camera);



        vector.x = (vector.x * widthHalf) + widthHalf;
        vector.y = -(vector.y * heightHalf) + heightHalf;
        return {
            x: vector.x,
            y: vector.y
        };

    }
    ;

    function showPopup(ele)
    {
        console.log("inside...")
      // var ele = e.currentTarget;
        var target = $(ele).attr("data-target");
        var hotspot_name = $(ele).attr('data-hotspot-name');
        // var rect = ele.getBoundingClientRect();
        // var top = rect.top;
        // var left = rect.left;
        // left = left +50;
        $("#info-popup").show();
        // $("#info-popup").css("top",top);
        // $("#info-popup").css("left",left);
        $(".infoPoint").hide();
        $("."+"infoPoint_"+target).show();
      //  console.log( $("."+"infoPoint_"+target));
      var vv_activity = {click:"information_point_click", vv_value: hotspot_name};
         storeAnalyticsActivity(vv_activity);
    }

    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top,
            width: rect.right,
            height: rect.bottom
        };
    }

    function getSphereShift(frameWidth, mouseVPos) {
        var frameVCenter = frameWidth / 2;
        var distanceFromCenter = 0;
        var foo = 0.001;
        if (mouseVPos > frameVCenter) {
            distanceFromCenter = mouseVPos - frameVCenter;
            foo = 0.005;
        } else {
            distanceFromCenter = frameVCenter - mouseVPos;
            foo = -0.005;
        }
        return foo;
    }


    var vertArray = new Array();
    window.ondevicemotion = function (event) {
        var ax = event.accelerationIncludingGravity.x
        var ay = event.accelerationIncludingGravity.y
        var az = event.accelerationIncludingGravity.z
        var rotation = event.rotationRate;
        if (rotation != null) {
            var arAlpha = Math.round(rotation.alpha);
            var arBeta = Math.round(rotation.beta);
            var arGamma = Math.round(rotation.gamma);
        }

        //var message = "X: " + ax.toFixed(2) + " | Y: " + ay.toFixed(2) + " | Z: " + az.toFixed(2);

        var fix = 1;
        if (whichMobile.iOS()) {
            fix = 1;
        } else {
            fix = -1;
        }

        verticalshift = az * 0.1 * fix;
        //verticalshift = verticalshift - (verticalshift % 0.05);

        if (vertArray.length >= 10) {
            vertArray.splice(0, 1);
        }
        vertArray.push(verticalshift);
        var totalVert = 0;
        for (var i = 0; i < vertArray.length; i++) {
            totalVert += vertArray[i];
        }

        verticalshift = totalVert / 10;
        var message = "Z: " + verticalshift;
        //$("#message").html(message);

        var message1 = "arAlpha: " + arAlpha + " | arBeta: " + arBeta + " | arGamma: " + arGamma;
        //$("#message3").html(message1);
    }

    window.addEventListener('deviceorientation', function (evt) {

        var heading = null;
        if (evt.alpha !== null) {
            heading = yrotation = compassHeading(evt.alpha, evt.beta, evt.gamma);
            //$("#message").html(heading.toFixed(2) + " | " + verticalshift.toFixed(2) + " | " + lon + " | " + lat);
        }

        // Do something with 'heading'...

    }, false);
    function compassHeading(alpha, beta, gamma) {

        // Convert degrees to radians
        var alphaRad = alpha * (Math.PI / 180);
        var betaRad = beta * (Math.PI / 180);
        var gammaRad = gamma * (Math.PI / 180);
        // Calculate equation components
        var cA = Math.cos(alphaRad);
        var sA = Math.sin(alphaRad);
        var cB = Math.cos(betaRad);
        var sB = Math.sin(betaRad);
        var cG = Math.cos(gammaRad);
        var sG = Math.sin(gammaRad);
        // Calculate A, B, C rotation components
        var rA = -cA * sG - sA * sB * cG;
        var rB = -sA * sG + cA * sB * cG;
        var rC = -cB * cG;
        // Calculate compass heading
        var compassHeading = Math.atan(rA / rB);
        // Convert from half unit circle to whole unit circle
        if (rB < 0) {
            compassHeading += Math.PI;
        } else if (rA < 0) {
            compassHeading += 2 * Math.PI;
        }

        // Convert radians to degrees
        //compassHeading *= 180 / Math.PI;

        return compassHeading;
    }

    function gyroOnOff() {
        if (gyro) {
            $("#gyro").attr("src", "images/GSensor_Inactive.png");
            sphereDim.rotation.y = sphere.rotation.y = 0;
            camera.rotation.x = 0;
            verticalmanualshift = 0;
            gyro = false;
            //$("#joystic").show();
        } else {
            $("#gyro").attr("src", "images/GSensor_Active.png");
            gyro = true;
            //$("#joystic").hide();
        }
    }

    function shiftScene(direction) {
        switch (direction) {
            case 'up':
                verticalmanualshift += 0.05;
                break;
            case 'down':
                verticalmanualshift -= 0.05;
                break;
            case 'left':
                sphereshift = 0.005;
                break;
            case 'right':
                sphereshift = -0.005;
                break;
            default:
                break;
        }
    }


    function showMap() {
        $("#viewMap").hide();
        $("#viewpointmap").show();
    }

    function hideMap() {
        $("#viewpointmap").hide();
        $("#viewMap").show();
    }
    function showHelp() {
        $(".help").show();
        $("#help").hide();
    }

    function hideHelp() {
        $(".help").hide();
        $("#help").show();
    }

    var whichMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    document.addEventListener('mousewheel', onDocumentMouseWheel, false);
    document.addEventListener('DOMMouseScroll', onDocumentMouseWheel, false);
    function onDocumentMouseWheel(event) {
        // WebKit

        if (event.wheelDeltaY) {

            fov -= event.wheelDeltaY * 0.05;
            console.log(fov);
            // Opera / Explorer 9

        } else if (event.wheelDelta) {

            fov -= event.wheelDelta * 0.05;
            // Firefox

        } else if (event.detail) {

            fov += event.detail * 1.0;
        }
        if (fov < 20) {
            fov = 20;
        }
        if (fov > 110) {
            fov = 110;
        }
        updateCameraProjection();
        //camera.projectionMatrix.makePerspective(fov, window.innerWidth / window.innerHeight, 1, 1100);
        
    }

    window.addEventListener('resize', onWindowResized, false);
    function onWindowResized(event) {

        renderer.setSize(window.innerWidth, window.innerHeight);
        updateCameraProjection();
    }


    var onPointerDownPointerX = 0,
            onPointerDownPointerY = 0,
            lon = 0, onPointerDownLon = 0,
            lat = 0, onPointerDownLat = 0;
    if (!isMobile) {
        document.addEventListener('mousedown', onDocumentMouseDown, false);
                document.addEventListener('touchstart', onCanvastTouchStart, false);
    } else {
        //// touch event
        document.addEventListener('touchstart', onCanvastTouchStart, false);
    }

    function onDocumentMouseDown(event) {

        event.preventDefault();
        onPointerDownPointerX = event.clientX;
        onPointerDownPointerY = event.clientY;
        onPointerDownLon = sphere.rotation.y;
        onPointerDownLat = camera.rotation.x;
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('mouseup', onDocumentMouseUp, false);
    }

    function onDocumentMouseMove(event) {

        lon = ((event.clientX - onPointerDownPointerX) * 0.005) + parseFloat(onPointerDownLon);
                //console.log(lon);
                sphereDim.rotation.y = sphere.rotation.y = lon;
        var vshift = 0;
        if (camera.rotation.x <= 0.9 && camera.rotation.x >= -0.9) {
            vshift = lat = (event.clientY - onPointerDownPointerY) * -0.001 + onPointerDownLat;
        } else if (camera.rotation.x >= 0.9) {
            vshift = 0.9;
        } else if (camera.rotation.x <= -0.9) {
            vshift = -0.9;
        }

        camera.rotation.x = vshift;

    }

    function onDocumentMouseUp(event) {

        document.removeEventListener('mousemove', onDocumentMouseMove, false);
        document.removeEventListener('mouseup', onDocumentMouseUp, false);
    }

    function onCanvastTouchStart(event) {
//console.log("inside..");
     event.preventDefault();          
        onPointerDownPointerX = event.changedTouches[0].clientX;
        onPointerDownPointerY = event.changedTouches[0].clientY;
        onPointerDownLon = sphere.rotation.y;
        onPointerDownLat = camera.rotation.x;
        document.addEventListener('touchmove', onCanvasTouchMove, false);
        document.addEventListener('touchend', onCanvasTouchEnd, false);
    }

    function onCanvasTouchMove(event) {
event.preventDefault();
        lon = (event.changedTouches[0].clientX - onPointerDownPointerX) * 0.005 + parseFloat(onPointerDownLon);
        sphereDim.rotation.y = sphere.rotation.y = lon;
        var vshift = 0;
        if (camera.rotation.x <= 0.9 && camera.rotation.x >= -0.9) {
            vshift = lat = (event.changedTouches[0].clientY - onPointerDownPointerY) * -0.001 + onPointerDownLat;
        } else if (camera.rotation.x >= 0.9) {
            vshift = 0.9;
        } else if (camera.rotation.x <= -0.9) {
            vshift = -0.9;
        }

        camera.rotation.x = vshift;

    }

    function onCanvasTouchEnd(event) {

        document.removeEventListener('touchmove', onCanvasTouchMove, false);
        document.removeEventListener('touchend', onCanvasTouchEnd, false);
    }

    var fovFixed = fov;
    if (isMobile) {
        document.addEventListener('gesturestart', function (e) {
            document.removeEventListener('touchstart', onCanvastTouchStart, false);
            document.removeEventListener('touchmove', onCanvasTouchMove, false);
            document.removeEventListener('touchend', onCanvasTouchEnd, false);

            fovFixed = fov;
        }, false);
        document.addEventListener('gestureend', function (e) {
            document.addEventListener('touchstart', onCanvastTouchStart, false);
        }, false);
        document.addEventListener('gesturechange', function (e) {
            var invertScale = 1;
            if (e.scale < 1.0) {
                // User moved fingers closer together
                invertScale = 1 + (1 - e.scale);
            } else if (e.scale > 1.0) {
                // User moved fingers further apart
                invertScale = 1 - (e.scale - 1);
            }

            fov = fovFixed * invertScale;
            if (fov < 20) {
                fov = 20;
            }
            if (fov > 110) {
                fov = 110;
            }

            $("#message").html("FOV: " + fov);
            updateCameraProjection();
        }, false);
    }



    function getMaterialFromFile(imageName, alpha = false) {

        var matPath = imageName;
        var textureMap = new THREE.TextureLoader().load(matPath);
        //textureMap.wrapS = textureMap.wrapT = THREE.RepeatWrapping;
        textureMap.anisotropy = 16;
        textureMap.minFilter = THREE.LinearFilter;
        if(!alpha){
        var matScene = new THREE.MeshBasicMaterial({map: textureMap, side: THREE.DoubleSide, });
        }
        else{
            var matScene = new THREE.MeshBasicMaterial({color: new THREE.Color( 0x0F7140 ), alphaMap:textureMap, side: THREE.DoubleSide, transparent: true});
        }
        return matScene;
    }




    function searchSceneInfo(viewpoint) {
//                console.log(data.scenes);
        viewpoint = parseInt(viewpoint);
        for (var i = 0; i < data.scenes.length; i++) {
            let id = data.scenes[i].scene_id;
            id = parseInt(id);
            if (id == viewpoint) {
                return data.scenes[i];
            }
        }
    }


    function loadScene(viewpoint, autoset) {
       // console.log(viewpoint);
        
         //console.log(ele);

         
        //   var scope = angular.element(document.getElementById('floorplan-container')).scope();
        //   scope.togglefloorplans(dat_floor);
        
            var ele = ".btn-active-" + viewpoint;
            var tp = parseInt($(ele).css("top"), 10) + 10;
           // console.log(tp);
            $("#people").css("top", tp + "px");

            var lt = parseInt($(ele).css("left"), 10) + 10;
            $("#people").css("left", lt + "px");
          //  console.log(lt);
            
            var scene_title = $(ele).attr('title');
            $("#people").attr('title', scene_title);

            var dat_floor = $(ele).attr("data-floor");
            window.activeFloor = "plan_" + dat_floor;
            // info = false;
            // $("#info-off").show();
            // $("#info-on").hide();
           
        
       
       

      
        var obj, i;

//              
        for (i = sphere.children.length - 1; i >= 0; i--) {
            obj = sphere.children[ i ];
            sphere.remove(obj);
          
        }

        var currentmaterial;
        var currentscene;
        var roomname;
        var sprotation = 0;


        var sceneInfo = searchSceneInfo(viewpoint);
        window.sceneInfo = searchSceneInfo(viewpoint);
       
        //console.log(sceneInfo);

      angular.element($("#floorplan-container")).scope().togglefloorplans(sceneInfo.floor);
      //resizedfloorplan();
      angular.element($("#floorplan-container")).scope().$apply();

      //// tracker code starts  //////////

        if(window.room_label_clicked == true){
            var vvdata = {"action": "vv_activity", "page":"interior", "vv_action":'room_hotspot_click', "house_type":window.house_type ,"room_name":sceneInfo.scene_label, "vv_value":"", "selfnav_id":window.selfnav_id, "scene_id":sceneInfo.scene_id,"environment":window.environment};

             $.ajax({url: window.ajaxurl, 
                  method:"POST", 
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data:$.param(
                      vvdata
                  )
              });
    }

    if(window.floorplan_label_clicked == true){
            var vvdata = {"action": "vv_activity", "page":"interior", "vv_action":'floorplan_hotspot_click', "house_type":window.house_type ,"room_name":sceneInfo.scene_label, "vv_value":"", "selfnav_id":window.selfnav_id, "scene_id":sceneInfo.scene_id, "environment":window.environment};

             $.ajax({url: window.ajaxurl, 
                  method:"POST", 
                  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  data:$.param(
                      vvdata
                  )
              });
    }

    //// tracker code ends///////////////
     

       //$("#people").html(sceneInfo.scene_id);
        $("#dim-room-name").html(sceneInfo.scene_label);
        $("#roomname").html(sceneInfo.scene_label);
        $("#width-imperial").html(sceneInfo.width_in_imperial);
        $("#length-imperial").html(sceneInfo.length_in_imperial);
        $("#height-imperial").html(sceneInfo.height_in_imperial);
        $("#width-metric").html(sceneInfo.width_in_metric);
        $("#length-metric").html(sceneInfo.length_in_metric);
        $("#height-metric").html(sceneInfo.height_in_metric);

        $("#area-imperial").html(sceneInfo.total_area_in_imperial);
        $("#area-metric").html(sceneInfo.total_area_in_metric);
        if(furniture){
                    currentmaterial = getMaterialFromFile(sceneInfo.scene_image);
                   
                }else{
                        currentmaterial = getMaterialFromFile(sceneInfo.scene_image_wof);
                        
                }
                
                let dim_material = getMaterialFromFile(sceneInfo.scene_desktop_image_dim, true);

        currentscene = sceneInfo.scene_hotspots;
   //  console.log(sceneInfo.scene_hotspots);
        sprotation = sceneInfo.rotation;

        

        currentsceneno = sceneInfo.index;

     setTimeout(function(){ showKeyPlan(sceneInfo.scene_floorplan); }, 100);   

        sphereOpacity.material = sphere.material;
        sphereDim.material = dim_material;
        spOpacity = 1;
        sphereDim.rotation.y = sphereOpacity.rotation.y = sphere.rotation.y;
        sphereOpacity.rotation.x = 0;

        $("#roomname").html(sceneInfo.scene_label);
        $("#col-2").html(roomname);
        sphere.material = currentmaterial;

        //fov = 50;
        if (autoset) {
            fov = 90;
            //camera.rotation.x = 0;
            sphereOpacity.rotation.x = (camera.rotation.x * -1) - 0.15;
            camera.rotation.x = -0.15;
            updateCameraProjection();
            sphereDim.rotation.y = sphere.rotation.y = sprotation;
        }


        var left = parseInt($(".arrow").css("left"), 10) + 15;
        $(".camera").css("left", left + "px");
        var top = parseInt($(".arrow").css("top"), 10) + 15;
        $(".camera").css("top", top + "px");
        $("#col-2").html(roomname);
        sphere.material = currentmaterial;


        var myplane = new THREE.PlaneGeometry(0, 0, 0);
        var planematerial = new THREE.MeshBasicMaterial({color: 0xffffff});

        $("#labels").html("");
        $("#info-popup").html("");
       // $("svg").html("");



        for (i = 0; i < currentscene.length; i++) {

            // console.log("currentscene.length"+ currentscene.length);
            //    if(furniture==false && currentscene[i].type=='info'){
            //        continue;
            //    }                    


            var object = new THREE.Mesh(myplane, planematerial);

                    var dx = parseInt((currentscene[i].degree_x/10) - 180) * -1;
                    var dy = parseInt((currentscene[i].degree_y/10) - 90) * -1;

                  //  console.log(dx + " - " + dy);

                    
                    var planePos = toWorldPosition(dx, dy);                    
                    

                    object.position.x = planePos.x;
                    object.position.y = planePos.y;
                    object.position.z = planePos.z;
                    object.visible = false;

                   
                    object.scene = currentscene[i].scene_id;
                    object.hotspot_id = currentscene[i].hotspot_id;
                    object.type = 'plane';
                    object.infotype = currentscene[i].hotspot_type;
                    sphere.add(object);
                    

                    var direction = currentscene[i].alignment;

                    if(sceneMirroring == 1){
                        if(direction == 'left'){
                            direction = 'right';    
                        }else if(direction == 'right'){
                            direction = 'left';    
                        }                        
                    }

                    
            if(currentscene[i].hotspot_type == 'info'){

                $("#labels").append("<div class='info-shot'><div id='"+currentscene[i].hotspot_id+"' class='"+currentscene[i].hotspot_type+" "+direction+" label' data-target='"+currentscene[i].hotspot_id+"' data-hotspot-name='"+currentscene[i].hotspot_name+"' onclick='showPopup(this)'><img class='infoicon' src='./images/info.png'/></div></div>");
                        $("#info-popup").append("<div class='infoPoint_"+ currentscene[i].hotspot_id + " infoPoint' ><div class='info-heading'><div class='info-t'>Specification</div><div class='close-info' onclick='hideInfo();'><img src='./images/circle.png' style='width:30px;cursor:pointer;'/></div></div><div class='clearfix'></div><div class='info-name'>"+currentscene[i].hotspot_name+"</div> </div> </div>");
                    }else{                        
                        $("#labels").append("<div id='"+currentscene[i].hotspot_id+"' class='"+currentscene[i].hotspot_type+" "+direction+" label' data-target='"+currentscene[i].scene_id+"' onclick='loadSceneFromLabel(this)'><div class='wrap' style='position:relative;'><span>"+currentscene[i].hotspot_name+"</span> <img class='flat-img' src='./images/"+direction+".png'/><img class='round-img' src='./images/h-l-img.png'/></div> </div>");
                    }
                   
        }
        console.log(info);
        if(info == true)
        {
           $(".info-shot").show();
        }
        else{
            $(".info-shot").hide();
        }

    }

    function resizedfloorplan()
    {
        // var fullwidth = 1200;
        // var displaywidth = $(".floorplan-inner").width();
        // console.log(displaywidth);
        // var ratio = (displaywidth / fullwidth);
        // var limitRatio = ratio.toFixed(2);
        // $(".floor-img").css("transform", "scale(" + limitRatio + "," + limitRatio + ")");
        // $(".floor-img").css("-webkit-transform", "scale(" + limitRatio + "," + limitRatio + ")");
        // $(".floor-img").css("-moz-transform", "scale(" + limitRatio + "," + limitRatio + ")");
       // $(".view").css("top", -top + "px");

       
    }

    function toWorldPosition(degX, degY) {
        var vector = new THREE.Vector3();

        var degreeX = THREE.Math.degToRad(degX);
        var degreeY = THREE.Math.degToRad(degY);

        var pSpRadX = 4.5; // Point Sphere X Radius
        var pSpRadY = Math.cos(degreeY) * pSpRadX;
        vector.x = Math.cos(degreeX) * pSpRadY;

        vector.y = Math.sin(degreeY) * pSpRadX;
        vector.z = Math.sin(degreeX) * pSpRadY;

        return vector;
    }

    function toMapPosition(coords) {
        var coords = coords.split(",");
        var newCoords = new Array();
        for (var i = 0; i < coords.length; i++) {
            var coord = coords[i] / 4; // if coords taken from image size 1440 (360 * 4)
            coord = Math.round(coord);
            if (i % 2 == 0) { // is even
                coord = 180 - coord;
            } else {
                coord = 90 - coord;
            }

            newCoords.push(coord);
        }


        return newCoords;
    }



    function loadSceneFromLabel(ele) {

        var scene = $(ele).attr("data-target");
        window.room_label_clicked = true;
        window.floorplan_label_clicked = false;
                loadScene(scene, true);

    }

    function loadSceneFromFloorplan(ele) {
        
                var scene = $(ele).attr("target");
                window.floorplan_label_clicked = true;
                window.room_label_clicked = false;  
                loadScene(scene, true);
            }

    /* Key Plan */

    function showKeyPlan(floor) {
        $(".btn-floor-nav").removeClass('active');
        if (floor == 0) {
            $("#ground-floor").show();
            $("#first-floor").hide();
            $("#btn-floor-nav-1").addClass('active');

            if (currentsceneno > 0) {
                $(".arrow").show();
               // $(".camera").show();
            } else {
                $(".arrow").hide();
                $(".camera").hide();
            }
        } else {
            //$("#ground-floor").hide();
            // $("#first-floor").show();
            $("#btn-floor-nav-2").addClass('active');

            if (currentsceneno < 0) {
                $(".arrow").hide();
                $(".camera").hide();
            } else {
                $(".arrow").show();
              //  $(".camera").show();
            }
        }
    }


    function fnGuideOnOff(status) {

        $(".btn-guide").removeClass('active');
        if (status == 1) {
            $("#user-guide-container").show();
            $("#btn-on").addClass('active');
        } else {
            $("#user-guide-container").hide();
            $("#btn-off").addClass('active');
        }
    }

    // function switchFurniture() {
                    
    //                 $('#furniture-on').hide();
    //                 $('.floor-wof').hide();
    //                 $('#furniture-off').show();
    //                 $('.floor-wf').show();
                    
                    
                
    //              if (furniture) {

    //                  $('#furniture-off').hide();
    //                  $('.floor-wf').hide();  
    //                  $('#furniture-on').show();
    //                  $('.floor-wof').show();
    //                  $('#dimension-on').show();

    //                  furniture = false;
                    
                    
    //              } else {
                     
    //                  $('#furniture-on').hide();
    //                  $('.floor-wof').hide();
    //                 $('#furniture-off').show();
    //                 $('.floor-wf').show();
    //                 $('#dimension-off').hide();
                   
    //                  furniture = true;
    //              }

    //             }


 function switchFurniture() {
                    console.log("furniture clicked");
                    $('#furniture-on').hide();
                    $('.floor-wof').hide();
                    $('#furniture-off').show();
                    $('.floor-wf').show();
                   
                    
                
                 if (furniture) {
                    console.log("fur on");
                      
                     $('#furniture-off').hide();
                     $('.floor-wf').hide();  
                     $('#furniture-on').show();
                     $('.floor-wof').show();
                    // $('#dimension-on-off').show();
                   
                     furniture = false;
                     
                    //  if($('#dimension-off').css('display') == 'block')
                    //  {
                    //     sphereDim.visible = true;
                    //  }
                    // else{
                    //     sphereDim.visible = false;
                    // }
                    var vv_activity = {click:"furniture_off_click"};
                   storeAnalyticsActivity(vv_activity);
                 } else {
                    console.log("fur off");
                  //  sphereDim.visible = false;
                     $('#furniture-on').hide();
                     $('.floor-wof').hide();
                    $('#furniture-off').show();
                    $('.floor-wf').show();
                   // $('#dimension-on-off').hide();
                   var vv_activity = {click:"furniture_on_click"};
                   storeAnalyticsActivity(vv_activity);
                    
                     furniture = true;
                    
                 }
                
                


 //console.log(parent.furniture);
             loadScene(data.scenes[currentsceneno].scene_id, false);
         }


function switchInfo()
{
    console.log(info);
    if (info) {
        $("#info-off").show();
        $("#info-on").hide();
        $(".info-shot").hide();
        info = false;
        var vv_activity = {click:"information_point_off_click"};
        storeAnalyticsActivity(vv_activity);
     }
     else{
        $("#info-on").show();
        $("#info-off").hide();
        $(".info-shot").show();
        info = true;
        var vv_activity = {click:"information_point_on_click"};
        storeAnalyticsActivity(vv_activity);
     }
}
    $('#info-box').click(function (e) {

        $('.info-box').hide();
    });
    function hideInfo() {
        $('.info-box').hide();
    }


 $(document).ready(function(){
           $('body').click(function(e){   

           // console.log(e);
            if (!$(e.target).is('#info-popup') && !$(e.target).is('.infoicon')){
               $('#info-popup').hide();
                }
            });
      });  

  function cache_images(response){
        var images = [];
        var images_floorplans = [];
       
        var scenes = response.scenes;
        var selfnav = response.selfnav;
        var selfnav_floorplans = selfnav.floorplans;
       //console.log(selfnav.floorplans);
       
        for(var i=0; i<scenes.length; i++){
            var scene_img = scenes[i].scene_image;
            if(scene_img != "" && scene_img != null){
                images.push(scene_img);                   
            }

            var scene_desktop_img_dim = scenes[i].scene_desktop_image_dim;
                if(scene_desktop_img_dim != "" && scene_desktop_img_dim != null){
                        images.push(scene_desktop_img_dim);                   
                }
            
            var scene_mobile_image_dim = scenes[i].scene_mobile_image_dim;
                if(scene_mobile_image_dim != "" && scene_mobile_image_dim != null){
                        images.push(scene_mobile_image_dim);                   
                }

            var scene_image_wof = scenes[i].scene_image_wof;
                if(scene_image_wof != "" && scene_image_wof != null){
                        images.push(scene_image_wof);                   
                }

        } // end for scenes

        for(var i=0; i<selfnav_floorplans.length; i++){
            var floorplan_image = selfnav_floorplans[i].floorplan_image;
                if(floorplan_image != "" && floorplan_image != null){
                    images.push(floorplan_image);                   
                }

            var floorplan_image_wof = selfnav_floorplans[i].floorplan_image_wof;
                if(floorplan_image_wof != "" && floorplan_image_wof != null){
                        images.push(floorplan_image_wof);                   
                }

        }
    
         
        //console.log(images);

        // setInterval(function(){
        //     $.imgpreload(images, function (){}); // end of image pre load
        // }, 2000); 

        var no_images_loaded = 0;
        var no_of_images = images.length; 

          $.imgpreload(images,
                    {

                         each:function(){
                            no_images_loaded++;
                        
                            var percentage = (no_images_loaded / no_of_images) * 100;
                            percentage = Math.round(percentage);
                        
                            $('#loader-perc').attr('data-pct',percentage);  

                            var degree = (no_images_loaded / no_of_images) * 360;
                            var pathElement = document.getElementById("circular-loader-exterior")
                            if (!pathElement) 
                            {
                                return; // don't do the rest
                            }
                            pathElement.setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, degree));
                            },
                         all:function ()
                        {
                           
                                $("#screen-loader-exterior").hide();
                                loadScene(data.scenes[0].scene_id, true);

                            //     <?php
                            // if ($view == "") {
                            //     ?>
                            //     loadScene(data.scenes[0].scene_id, true);
                                
                            // <?php } else { ?>
                            //     var viewindex = searchViewIndex(<?= $view ?>);
                            //     loadScene(data.scenes[viewindex].scene_id, true);
                            // <? } ?>
                          
                                
                        }
         });         
        
        
    }// end of cache_images function

function LoadImages(){
    // Getting Image src

     var imageDOMs = document.images;
     //console.log(imageDOMs);
          var images = [];

        for(var i=0; i<imageDOMs.length; i++){
            //console.log($(imageDOMs[i]).attr("src"));
            if($(imageDOMs[i]).attr("src") != undefined)
            {
                 images.push($(imageDOMs[i]).attr("src"));
            }
             
             //console.log("img->"+$(imageDOMs).attr("src"));
          }   
        
          //console.log(images);
  
          // Getting Background images
          var BackgroundImageDOMs = $("div");
          for(var i=0; i<BackgroundImageDOMs.length; i++){
              var bg = $(BackgroundImageDOMs[i]).css("background-image");   
              //console.log(bg);                     
                  if(bg && bg!='none' && bg!='undefined'){
                bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
                var extension = bg.slice(bg.length - 3);
                   //console.log("bg->"+bg);
                    images.push(bg);
                } 
              
          }

                images.push('./images/info.png');
                images.push('./images/circle.png');
                images.push('./images/left.png');
                images.push('./images/right.png');
                images.push('./images/top.png');
                images.push('./images/bottom.png');

         //console.log(images);
      $.imgpreload(images,function(){   
       });
  }

  function storeAnalyticsActivity(activity){
        var vv_value;
        if(activity.vv_value)
       {
            vv_value = activity.vv_value;
        }else{
            vv_value = "";
        }
        var vvdata = {"action": "vv_activity", "page":"interior", "vv_action":activity.click, "house_type":window.house_type ,"room_name":window.sceneInfo.scene_label, "vv_value":vv_value, "selfnav_id":window.selfnav_id, "scene_id":window.sceneInfo.scene_id, "environment":window.environment};
    
              $.ajax({url: window.ajaxurl, 
                    method:"POST", 
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data:$.param(
                                      vvdata
                                  )
                });
    }
