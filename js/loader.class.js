class Loader{

    static polar_to_cartesian(center_x, center_y, radius, angle_in_degrees) {
        var angle_in_radians = (angle_in_degrees-90) * Math.PI / 180.0;
      
        return {
          x: center_x + (radius * Math.cos(angle_in_radians)),
          y: center_y + (radius * Math.sin(angle_in_radians))
        };
      }
      
      static circular_loader(x, y, radius, start_angle, end_angle){
      
          var start = this.polar_to_cartesian(x, y, radius, end_angle);
          var end = this.polar_to_cartesian(x, y, radius, start_angle);
      
          var large_arc_flag = end_angle - start_angle <= 180 ? "0" : "1";
      
          var d = [
              "M", start.x, start.y, 
              "A", radius, radius, 0, large_arc_flag, 0, end.x, end.y
          ].join(" ");
      
          return d;       
      }
    
    }