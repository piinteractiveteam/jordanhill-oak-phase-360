// (function(){
//     //alert(window.navigator.userAgent.indexOf('MSIE '));
//     var ie = IeVersion();
//   //  alert('IsIE: ' + ie.IsIE + '\nVersion: ' + ie.TrueVersion + '\nCompatability On: ' + ie.CompatibilityMode);

//              if (ie.IsIE === false) {
//                     if(navigator.onLine)
//                     {
//                     //alert('Other browser');
//                                 jQuery.post(
//                                     ajax_object.ajaxurl,
//                                     {
//                                         'action': 'get_all_plots_data',
                                        
//                                     },
//                                     function (response)
//                                     { 
//                                         cache_images(response);
                                    
//                                     }
//                                 );
//                     }
//                     else{
                        
//                     }         
//                 }else{
//                     var site_url = $("#site-url").val();
//                     // console.log(site_url + "/homeselector");
//                      window.open(site_url + '/page-for-explorer','_self');
//                 }   
// })();


// function IeVersion() {
//     //Set defaults
//     var value = {
//         IsIE: false,
//         TrueVersion: 0,
//         ActingVersion: 0,
//         CompatibilityMode: false
//     };

//     //Try to find the Trident version number
//     var trident = navigator.userAgent.match(/Trident\/(\d+)/);
//     if (trident) {
//         value.IsIE = true;
//         //Convert from the Trident version number to the IE version number
//         value.TrueVersion = parseInt(trident[1], 10) + 4;
//     }

//     //Try to find the MSIE number
//     var msie = navigator.userAgent.match(/MSIE (\d+)/);
//     if (msie) {
//         value.IsIE = true;
//         //Find the IE version number from the user agent string
//         value.ActingVersion = parseInt(msie[1]);
//     } else {
//         //Must be IE 11 in "edge" mode
//         value.ActingVersion = value.TrueVersion;
//     }

//     //If we have both a Trident and MSIE version number, see if they're different
//     if (value.IsIE && value.TrueVersion > 0 && value.ActingVersion > 0) {
//         //In compatibility mode if the trident number doesn't match up with the MSIE number
//         value.CompatibilityMode = value.TrueVersion != value.ActingVersion;
//     }
//     return value;
// }

function cache_all_images(response){
    var images = [];
    var images_finishes = [];
    var images_floorplans = [];
    var units = JSON.parse(response);
    //console.log(units);
    for(var i=0; i<units.units.length; i++){
        var img = units.units[i].unit_image;
        if(img != "" && img != null)
        {
            images.push(img);                   
        }// master house image

        var devImage = units.units[i].unit_type_image;
          if(devImage != "" && devImage != null)
          {
            images.push(devImage);                   
          }// development house images

        var gallaryImage = units.units[i].gallary_image;
        if(gallaryImage != "" && gallaryImage != null)
        {
          images.push(gallaryImage);                   
        }// gallary house images


        var gallery = units.units[i].gallery;
        if(gallery != null){
            for(var f=0; f<gallery.length; f++){
                var img = gallery[f].image_url;
                if(img != "" && img != null){
                    images.push(img);                   
                }
            } // end for gallery
        } // end if gallery


        var plotplanImage = units.units[i].location_plan_image;
        if(plotplanImage != "" && plotplanImage != null)
        {
          images.push(plotplanImage);                   
        }// plotplan Images


        var finishes = units.units[i].finishes;
        if(finishes != null){
            for(var f=0; f<finishes.length; f++){
                var img = finishes[f].finish_url;
                if(img != "" && img != null){
                    images.push(img);                   
                }
            } // end for finishes
        } // end if finishes

        var floorplans = units.units[i].floorplans;
        if(floorplans != null){
            for(var p=0; p<floorplans.length; p++){
                var img = floorplans[p].plan_25d;
                if(img != "" && img != null){
                    images.push(img);                   
                }
                img = floorplans[p].plan_2d;
                if(img != "" && img != null){
                    images.push(img);                   
                }
            } // end for floorplans
        } // end if floorplans

    } // end for units
    //console.log(images);
    

    var no_images_loaded = 0;
    var no_of_images = images.length;

    $.imgpreload(images, {
        each:function(){
            no_images_loaded++;
          
            var percentage = (no_images_loaded / no_of_images) * 100;
            percentage = Math.round(percentage);
         
            $('#loader-perc').attr('data-pct',percentage);  

            var degree = (no_images_loaded / no_of_images) * 360;
            degree = degree - 0.01;
            
            document.getElementById("circular-loader").setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, degree));
        },
        all:function ()
       {
        
        
        var site_url = $("#site-url").val();
        $(".header-box").css("border-radius", "0px");
        window.open(site_url + '/homeselector','_self');
       
      
      }
   

});
//window.location = "http://192.168.0.30/homeselector_macmic/haddington/homeselector/";
}// end of cache image


var degree = 0;
document.getElementById("circular-gray").setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, 359.99));
document.getElementById("circular-ring").setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, 359.99));
