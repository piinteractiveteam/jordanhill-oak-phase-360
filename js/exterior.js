
    var furniture = false;

    Math.degrees = function (radians) {
        return radians * 180 / Math.PI;
    };
    var currentsceneno = 0;

    function fnNextScene() {


        currentsceneno++;
        if (currentsceneno >= data.scenes.length) {
            currentsceneno = 0;
        }
        //      
        loadScene(data.scenes[currentsceneno].scene_no, true);

    }

    function fnPreviousScene() {
        currentsceneno--;
        if (currentsceneno < 0) {
            currentsceneno = data.scenes.length - 1;
        }

        loadScene(data.scenes[currentsceneno].scene_no, true);

    }

    
    var data;
    var scene = new THREE.Scene();
    var sphereshift = 0.001;
    var yrotation = 0;
    var verticalshift = 0;
    var verticalmanualshift = 0;
    var gyro = false;
    var fov = 60;
    var camera = new THREE.PerspectiveCamera(fov, window.innerWidth / window.innerHeight, 0.1, 1000);
    var mouse = new THREE.Vector2(), INTERSECTED, intersects;
    var raycaster;
    var spOpacity = 1;
    var frustum = new THREE.Frustum();
    var cameraViewProjectionMatrix = new THREE.Matrix4();
    var SCREEN_WIDTH = window.innerWidth;
	var SCREEN_HEIGHT = window.innerHeight;
	var aspect = SCREEN_WIDTH / SCREEN_HEIGHT;

    var sceneMirroring = -1; //default -1 and for mirror 1


    var light = new THREE.DirectionalLight(0xffffff, 1);
    light.position.set(1, 1, 1).normalize();
    scene.add(light);
    raycaster = new THREE.Raycaster();
    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    $(document).ready(function () {

        document.body.appendChild(renderer.domElement);
        var canvass = document.getElementsByTagName('canvas');
        var canvas = canvass[0];
        var context = canvas.getContext('2d');
//        // codes...

    });



    var geometry = new THREE.SphereGeometry(5, 128, 128);
    var material = new THREE.MeshBasicMaterial({color: 0xffffff});
    var sphere = new THREE.Mesh(geometry, material);
    sphere.scale.x = sceneMirroring;
    scene.add(sphere);

    var geometryOpacity = new THREE.SphereGeometry(4.8, 128, 128);
    var materialOpacity = new THREE.MeshBasicMaterial({color: 0xffffff, opacity: spOpacity, transparent: true});
    var sphereOpacity = new THREE.Mesh(geometryOpacity, materialOpacity);
    sphereOpacity.scale.x = sceneMirroring;
    scene.add(sphereOpacity);

    camera.position.z = 0;
    camera.rotation.x = 0;
    var render = function () {

        requestAnimationFrame(render);
        raycaster.setFromCamera(mouse, camera);
        intersects = raycaster.intersectObjects(sphere.children);

        var spchild = sphere.children;
        for (var a = 0; a < spchild.length; a++) {

            if (spchild[a].type == 'plane') {
                showLabelOnScreen(spchild[a], camera);
            }

            if (spchild[a].type == 'group') {
                showPolygonsOnScreen(spchild[a], camera);
            }

        }

        if (gyro) {
            sphere.rotation.y = yrotation.toFixed(2);
            camera.rotation.x = verticalshift.toFixed(2);
        } else {
            //sphere.rotation.y += sphereshift;
            //sphere.rotation.y = 0;
            //camera.rotation.x = verticalmanualshift.toFixed(2);
        }


        var cameraRotation = Math.degrees(sphere.rotation.y) - 30;
        $(".camera").css("transform", "rotate(" + cameraRotation + "deg)");



        if (spOpacity > 0) {
            spOpacity -= 0.1; /* To manage Opacity Speed*/
            sphereOpacity.material.transparent = true;
            sphereOpacity.material.opacity = spOpacity;
        }

       // console.log(sphere.rotation.y);

        renderer.render(scene, camera);
    };
    render();


    function updateCameraProjection(){
        SCREEN_WIDTH = window.innerWidth;
				SCREEN_HEIGHT = window.innerHeight;
				aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
				renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
				camera.aspect = aspect;
				camera.updateProjectionMatrix();
    }

    function showPolygonsOnScreen(group, camera) {
        var points = group.children;
        var coords = new Array();
        var visibles = new Array();
        for (var i = 0; i < points.length; i++) {
            var obj = points[i];
            var vector = toScreenPosition(obj, camera);
            coords.push(Math.round(vector.x));
            coords.push(Math.round(vector.y));
            visibles.push(toScreenVisible(obj, camera));
        }

        var coordsStr = coords.toString();

        $("#poly_" + group.scene).attr("points", coordsStr);

        var visible = isGroupVisible(visibles);

        if (visible) {
            $("#poly_" + group.scene).show();
        } else {
            $("#poly_" + group.scene).hide();
        }

    }

    function isGroupVisible(visibles) {
        for (var i = 0; i < visibles.length; i++) {
            if (visibles[i]) {
                return true;
            }
        }

        return false;
    }

    function showLabelOnScreen(obj, camera) {
        // var vector = toScreenPosition(obj, camera);
        // var visible = toScreenVisible(obj, camera);

        // if (visible) {
        //     $("#tag_" + obj.idx).show();
        // } else {
        //     $("#tag_" + obj.idx).hide();
        // }

        // $("#tag_" + obj.idx)
        //         .css('left', (vector.x - $("#tag_" + obj.idx).width() / 2) + 'px')

        //         .css('top', (vector.y - $("#tag_" + obj.idx).height() / 2) + 'px');
        var vector = toScreenPosition(obj, camera);
        var visible = toScreenVisible(obj, camera);
        
        if(visible){

            $("#"+obj.hotspot_id).show();
        }else{
            $("#"+obj.hotspot_id).hide();
        }

        $("#"+obj.hotspot_id)
        .css('left', (vector.x - $("#"+obj.hotspot_id).width() / 2) + 'px')
        .css('top', (vector.y - $("#"+obj.hotspot_id).height() / 2) + 'px');
    }


    function toScreenVisible(obj, camera) {
        // every time the camera or objects change position (or every frame)

        camera.updateMatrixWorld(); // make sure the camera matrix is updated
        camera.matrixWorldInverse.getInverse(camera.matrixWorld);
        cameraViewProjectionMatrix.multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse);
        frustum.setFromMatrix(cameraViewProjectionMatrix);

        // frustum is now ready to check all the objects you need                
        var visible = frustum.intersectsObject(obj);


        return visible;
    }

    function toScreenPosition(obj, camera) {


        var vector = new THREE.Vector3();

        var widthHalf = 0.5 * renderer.context.canvas.width;
        var heightHalf = 0.5 * renderer.context.canvas.height;

        obj.updateMatrixWorld();

        vector.setFromMatrixPosition(obj.matrixWorld);
        vector.project(camera);



        vector.x = (vector.x * widthHalf) + widthHalf;
        vector.y = -(vector.y * heightHalf) + heightHalf;
        return {
            x: vector.x,
            y: vector.y
        };

    }
    ;



    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top,
            width: rect.right,
            height: rect.bottom
        };
    }

    function getSphereShift(frameWidth, mouseVPos) {
        var frameVCenter = frameWidth / 2;
        var distanceFromCenter = 0;
        var foo = 0.001;
        if (mouseVPos > frameVCenter) {
            distanceFromCenter = mouseVPos - frameVCenter;
            foo = 0.005;
        } else {
            distanceFromCenter = frameVCenter - mouseVPos;
            foo = -0.005;
        }
        return foo;
    }


    var vertArray = new Array();
    window.ondevicemotion = function (event) {
        var ax = event.accelerationIncludingGravity.x
        var ay = event.accelerationIncludingGravity.y
        var az = event.accelerationIncludingGravity.z
        var rotation = event.rotationRate;
        if (rotation != null) {
            var arAlpha = Math.round(rotation.alpha);
            var arBeta = Math.round(rotation.beta);
            var arGamma = Math.round(rotation.gamma);
        }

        //var message = "X: " + ax.toFixed(2) + " | Y: " + ay.toFixed(2) + " | Z: " + az.toFixed(2);

        var fix = 1;
        if (whichMobile.iOS()) {
            fix = 1;
        } else {
            fix = -1;
        }

        verticalshift = az * 0.1 * fix;
        //verticalshift = verticalshift - (verticalshift % 0.05);

        if (vertArray.length >= 10) {
            vertArray.splice(0, 1);
        }
        vertArray.push(verticalshift);
        var totalVert = 0;
        for (var i = 0; i < vertArray.length; i++) {
            totalVert += vertArray[i];
        }

        verticalshift = totalVert / 10;
        var message = "Z: " + verticalshift;
        //$("#message").html(message);

        var message1 = "arAlpha: " + arAlpha + " | arBeta: " + arBeta + " | arGamma: " + arGamma;
        //$("#message3").html(message1);
    }

    window.addEventListener('deviceorientation', function (evt) {

        var heading = null;
        if (evt.alpha !== null) {
            heading = yrotation = compassHeading(evt.alpha, evt.beta, evt.gamma);
            //$("#message").html(heading.toFixed(2) + " | " + verticalshift.toFixed(2) + " | " + lon + " | " + lat);
        }

        // Do something with 'heading'...

    }, false);
    function compassHeading(alpha, beta, gamma) {

        // Convert degrees to radians
        var alphaRad = alpha * (Math.PI / 180);
        var betaRad = beta * (Math.PI / 180);
        var gammaRad = gamma * (Math.PI / 180);
        // Calculate equation components
        var cA = Math.cos(alphaRad);
        var sA = Math.sin(alphaRad);
        var cB = Math.cos(betaRad);
        var sB = Math.sin(betaRad);
        var cG = Math.cos(gammaRad);
        var sG = Math.sin(gammaRad);
        // Calculate A, B, C rotation components
        var rA = -cA * sG - sA * sB * cG;
        var rB = -sA * sG + cA * sB * cG;
        var rC = -cB * cG;
        // Calculate compass heading
        var compassHeading = Math.atan(rA / rB);
        // Convert from half unit circle to whole unit circle
        if (rB < 0) {
            compassHeading += Math.PI;
        } else if (rA < 0) {
            compassHeading += 2 * Math.PI;
        }

        // Convert radians to degrees
        //compassHeading *= 180 / Math.PI;

        return compassHeading;
    }

    function gyroOnOff() {
        if (gyro) {
            $("#gyro").attr("src", "images/GSensor_Inactive.png");
            sphere.rotation.y = 0;
            camera.rotation.x = 0;
            verticalmanualshift = 0;
            gyro = false;
            //$("#joystic").show();
        } else {
            $("#gyro").attr("src", "images/GSensor_Active.png");
            gyro = true;
            //$("#joystic").hide();
        }
    }

    function shiftScene(direction) {
        switch (direction) {
            case 'up':
                verticalmanualshift += 0.05;
                break;
            case 'down':
                verticalmanualshift -= 0.05;
                break;
            case 'left':
                sphereshift = 0.005;
                break;
            case 'right':
                sphereshift = -0.005;
                break;
            default:
                break;
        }
    }


    function showMap() {
        $("#viewMap").hide();
        $("#viewpointmap").show();
    }

    function hideMap() {
        $("#viewpointmap").hide();
        $("#viewMap").show();
    }
    function showHelp() {
        $(".help").show();
        $("#help").hide();
    }

    function hideHelp() {
        $(".help").hide();
        $("#help").show();
    }

    var whichMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    document.addEventListener('mousewheel', onDocumentMouseWheel, false);
    document.addEventListener('DOMMouseScroll', onDocumentMouseWheel, false);
    function onDocumentMouseWheel(event) {

        // WebKit

        if (event.wheelDeltaY) {

            fov -= event.wheelDeltaY * 0.05;
            // Opera / Explorer 9

        } else if (event.wheelDelta) {

            fov -= event.wheelDelta * 0.05;
            // Firefox

        } else if (event.detail) {

            fov += event.detail * 1.0;
        }
        if (fov < 20) {
            fov = 20;
        }
        if (fov > 110) {
            fov = 110;
        }
        updateCameraProjection();
    }

    window.addEventListener('resize', onWindowResized, false);
    function onWindowResized(event) {

        renderer.setSize(window.innerWidth, window.innerHeight);
        updateCameraProjection();
    }


    var onPointerDownPointerX = 0,
            onPointerDownPointerY = 0,
            lon = 0, onPointerDownLon = 0,
            lat = 0, onPointerDownLat = 0;
            document.addEventListener('mousedown', onDocumentMouseDown, false);
            document.addEventListener('touchstart', onCanvastTouchStart, false);
//     if (!isMobile) {
//         document.addEventListener('mousedown', onDocumentMouseDown, false);
// //                 canvas.addEventListener('touchstart', onCanvastTouchStart, false);
//     } else {
//         //// touch event
//         document.addEventListener('touchstart', onCanvastTouchStart, false);
//     }

    function onDocumentMouseDown(event) {

        event.preventDefault();
        onPointerDownPointerX = event.clientX;
        onPointerDownPointerY = event.clientY;
        onPointerDownLon = sphere.rotation.y;
        onPointerDownLat = camera.rotation.x;
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('mouseup', onDocumentMouseUp, false);
    }

    function onDocumentMouseMove(event) {

        lon = ((event.clientX - onPointerDownPointerX) * 0.005) + parseFloat(onPointerDownLon);
                console.log("in");
                sphere.rotation.y = lon;
        var vshift = 0;
        if (camera.rotation.x <= 0.9 && camera.rotation.x >= -0.9) {
            vshift = lat = (event.clientY - onPointerDownPointerY) * -0.001 + onPointerDownLat;
        } else if (camera.rotation.x >= 0.9) {
            vshift = 0.9;
        } else if (camera.rotation.x <= -0.9) {
            vshift = -0.9;
        }

        camera.rotation.x = vshift;

    }

    function onDocumentMouseUp(event) {

        document.removeEventListener('mousemove', onDocumentMouseMove, false);
        document.removeEventListener('mouseup', onDocumentMouseUp, false);
    }

    function onCanvastTouchStart(event) {

//                event.preventDefault();
        onPointerDownPointerX = event.changedTouches[0].clientX;
        onPointerDownPointerY = event.changedTouches[0].clientY;
        onPointerDownLon = sphere.rotation.y;
        onPointerDownLat = camera.rotation.x;
        document.addEventListener('touchmove', onCanvasTouchMove, false);
        document.addEventListener('touchend', onCanvasTouchEnd, false);
    }

    function onCanvasTouchMove(event) {
console.log("in");
        lon = (event.changedTouches[0].clientX - onPointerDownPointerX) * 0.005 + parseFloat(onPointerDownLon);
                sphere.rotation.y = lon;
        var vshift = 0;
        if (camera.rotation.x <= 0.9 && camera.rotation.x >= -0.9) {
            vshift = lat = (event.changedTouches[0].clientY - onPointerDownPointerY) * -0.001 + onPointerDownLat;
        } else if (camera.rotation.x >= 0.9) {
            vshift = 0.9;
        } else if (camera.rotation.x <= -0.9) {
            vshift = -0.9;
        }

        camera.rotation.x = vshift;

    }

    function onCanvasTouchEnd(event) {

        document.removeEventListener('touchmove', onCanvasTouchMove, false);
        document.removeEventListener('touchend', onCanvasTouchEnd, false);
    }

    var fovFixed = fov;
    if (isMobile) {
        document.addEventListener('gesturestart', function (e) {
            document.removeEventListener('touchstart', onCanvastTouchStart, false);
            document.removeEventListener('touchmove', onCanvasTouchMove, false);
            document.removeEventListener('touchend', onCanvasTouchEnd, false);

            fovFixed = fov;
        }, false);
        document.addEventListener('gestureend', function (e) {
            document.addEventListener('touchstart', onCanvastTouchStart, false);
        }, false);
        document.addEventListener('gesturechange', function (e) {
            var invertScale = 1;
            if (e.scale < 1.0) {
                // User moved fingers closer together
                invertScale = 1 + (1 - e.scale);
            } else if (e.scale > 1.0) {
                // User moved fingers further apart
                invertScale = 1 - (e.scale - 1);
            }

            fov = fovFixed * invertScale;
            if (fov < 20) {
                fov = 20;
            }
            if (fov > 110) {
                fov = 110;
            }

            $("#message").html("FOV: " + fov);
            updateCameraProjection();
        }, false);
    }



    function getMaterialFromFile(imageName) {

        var matPath = imageName;
        var textureMap = new THREE.TextureLoader().load(matPath);
        //textureMap.wrapS = textureMap.wrapT = THREE.RepeatWrapping;
        textureMap.anisotropy = 16;
        textureMap.minFilter = THREE.LinearFilter;
        var matScene = new THREE.MeshBasicMaterial({map: textureMap, side: THREE.DoubleSide});
        return matScene;
    }

    function searchSceneInfo(viewpoint) {
            //  console.log(data.scenes);
        for (var i = 0; i < data.scenes.length; i++) {
            if (data.scenes[i].scene_no == viewpoint) {
                return data.scenes[i];
            }
        }
    }


    function loadScene(viewpoint, autoset) {
      //  console.log(viewpoint);
        var ele = "#btn-" + viewpoint;

        var tp = parseInt($(ele).css("top"), 10) + 10;
        $("#people").css("top", tp + "px");

        var lt = parseInt($(ele).css("left"), 10) + 10;
        $("#people").css("left", lt + "px");

        
        var scene_title = $(ele).attr('title');
        $("#people").attr('title', scene_title);

        var obj, i;

//              
        for (i = sphere.children.length - 1; i >= 0; i--) {
            obj = sphere.children[ i ];
            sphere.remove(obj);
      
        }

        var currentmaterial;
        var currentscene;
        var roomname;
        var sprotation = 0;


        var sceneInfo = searchSceneInfo(viewpoint);
       // console.log(sceneInfo);

        currentmaterial = getMaterialFromFile(sceneInfo.scene_image);


        currentscene = sceneInfo.scene_hotspots;
//               console.log(sceneInfo.scene_coordinates);
        sprotation = sceneInfo.rotation;

        

        currentsceneno = sceneInfo.index;
        showKeyPlan(sceneInfo.scene_floorplan);

        sphereOpacity.material = sphere.material;
        spOpacity = 1;
        sphereOpacity.rotation.y = sphere.rotation.y;
        sphereOpacity.rotation.x = 0;

        $("#roomname").html(sceneInfo.scene_label);
        $("#col-2").html(roomname);
        sphere.material = currentmaterial;

        //fov = 50;
        if (autoset) {
            fov = 65;
            camera.rotation.x = 0;
            sphereOpacity.rotation.x = (camera.rotation.x * -1) - 0.15;
            //camera.rotation.x = -0.15;
            updateCameraProjection();
            sphere.rotation.y = sprotation;
        }


        var left = parseInt($(".arrow").css("left"), 10) + 15;
       // console.log("left="+left);
        $(".camera").css("left", left + "px");
        var top = parseInt($(".arrow").css("top"), 10) + 15;
        $(".camera").css("top", top + "px");
        $("#col-2").html(roomname);
        sphere.material = currentmaterial;


        var myplane = new THREE.PlaneGeometry(0, 0, 0);
        var planematerial = new THREE.MeshBasicMaterial({color: 0xffffff});

        $("#labels").html("");
        $("svg").html("");



        for (i = 0; i < currentscene.length; i++) {

            // console.log("currentscene.length"+ currentscene.length);
            //    if(furniture==false && currentscene[i].type=='info'){
            //        continue;
            //    }                    


            var object = new THREE.Mesh(myplane, planematerial);
            var dx = parseInt((currentscene[i].degree_x/10) - 180) * -1;
            var dy = parseInt((currentscene[i].degree_y/10) - 90) * -1;

            var planePos = toWorldPosition(dx, dy);

           // var planePos = toWorldPosition(currentscene[i].degX, currentscene[i].degY);
            object.position.x = planePos.x;
            object.position.y = planePos.y;
            object.position.z = planePos.z;


            object.scene = currentscene[i].scene_id;
            object.hotspot_id = currentscene[i].hotspot_id;
            object.idx = i;

            object.type = 'plane';
            object.infotype = currentscene[i].hotspot_type;
            object.visible = false;
            //console.log(object);
            sphere.add(object);


            var direction = currentscene[i].alignment;

            if (sceneMirroring == 1) {
                if (direction == 'left') {
                    direction = 'right';
                } else if (direction == 'right') {
                    direction = 'left';
                }
            }

            if (currentscene[i].hotspot_type == 'info') {
                $("#labels").append("<div id='" + currentscene[i].hotspot_id + "' class='" + currentscene[i].hotspot_type + " " + direction + " label'   data-target='" + currentscene[i].scene + "'data-info='" + currentscene[i].unit_num + "' onclick='parent.open_popup_from_selfnav(" + currentscene[i].unit_num + ", this.parentElement ,this)'><div class='info-square' style='background-color: " + currentscene[i].availability + ";'><div class='info-unit'>" + currentscene[i].unit_num + "</div></div><div class='hotspot-locator-info hotspot-locator' style='background-repeat:no-repeat;'>  </div> </div>");

            } else if(currentscene[i].hotspot_type == 'message'){
                $("#labels").append("<div id='" + currentscene[i].hotspot_id + "' class='" + currentscene[i].hotspot_type + " " + direction + " label'>" + currentscene[i].hotspot_name +"</div>");
            } else {
//                       
                $("#labels").append("<div id='" + currentscene[i].hotspot_id + "' class='" + currentscene[i].hotspot_type + " " + direction + " label' data-target='" + currentscene[i].scene_no + "' onclick='loadSceneFromLabel(this)'><div class='hotspot-locator' style='background-image:url(images/icon-location-360.png);background-repeat:no-repeat;'><div class='title-disable-hotspot'>" + currentscene[i].scene_no + "</div>  </div> </div>");
            }

        }

    }



    function toWorldPosition(degX, degY) {
        var vector = new THREE.Vector3();

        var degreeX = THREE.Math.degToRad(degX);
        var degreeY = THREE.Math.degToRad(degY);

        var pSpRadX = 4.5; // Point Sphere X Radius
        var pSpRadY = Math.cos(degreeY) * pSpRadX;
        vector.x = Math.cos(degreeX) * pSpRadY;

        vector.y = Math.sin(degreeY) * pSpRadX;
        vector.z = Math.sin(degreeX) * pSpRadY;

        return vector;
    }

    function toMapPosition(coords) {
        var coords = coords.split(",");
        var newCoords = new Array();
        for (var i = 0; i < coords.length; i++) {
            var coord = coords[i] / 4; // if coords taken from image size 1440 (360 * 4)
            coord = Math.round(coord);
            if (i % 2 == 0) { // is even
                coord = 180 - coord;
            } else {
                coord = 90 - coord;
            }

            newCoords.push(coord);
        }


        return newCoords;
    }



    function loadSceneFromLabel(ele) {

        var scene = $(ele).attr("data-target");

        loadScene(scene, true);

    }

    function showPopup(ele) {

        var scene = $(ele).attr("data-info");
//        console.log("unit no=" + scene);

//                loadScene(scene, true);

    }




    /* Key Plan */

    function showKeyPlan(floor) {
        $(".btn-floor-nav").removeClass('active');
        if (floor == 0) {
            $("#ground-floor").show();
            $("#first-floor").hide();
            $("#btn-floor-nav-1").addClass('active');

            if (currentsceneno > 0) {
                $(".arrow").show();
                $(".camera").show();
            } else {
                $(".arrow").hide();
                $(".camera").hide();
            }
        } else {
            //$("#ground-floor").hide();
            // $("#first-floor").show();
            $("#btn-floor-nav-2").addClass('active');

            if (currentsceneno < 0) {
                $(".arrow").hide();
                $(".camera").hide();
            } else {
                $(".arrow").show();
                $(".camera").show();
            }
        }
    }


    function fnGuideOnOff(status) {

        $(".btn-guide").removeClass('active');
        if (status == 1) {
            $("#user-guide-container").show();
            $("#btn-on").addClass('active');
        } else {
            $("#user-guide-container").hide();
            $("#btn-off").addClass('active');
        }
    }
    function switchFurniture() {
        if (furniture) {
            furniture = false;
            $(".btn-furniture").html("Show");
            $("#furniture-icon").attr("src", "images/furniture-off.png");
        } else {
            furniture = true;
            $(".btn-furniture").html("Hide");
            $("#furniture-icon").attr("src", "images/furniture-on.png");
        }

        loadScene(data.scenes[currentsceneno].scene_no, false);
    }


    $('#info-box').click(function (e) {

        $('.info-box').hide();
    });
    function hideInfo() {
        $('.info-box').hide();
    }

     function cache_images(response){
        var images = [];
        var images_floorplans = [];
       
        var scenes = response.scenes;
        var selfnav = response.selfnav;
        var selfnav_floorplans = selfnav.floorplans;
       //console.log(selfnav.floorplans);
       
        for(var i=0; i<scenes.length; i++){
            var scene_img = scenes[i].scene_image;
            if(scene_img != "" && scene_img != null){
                images.push(scene_img);                   
            }

            var scene_desktop_img_dim = scenes[i].scene_desktop_image_dim;
                if(scene_desktop_img_dim != "" && scene_desktop_img_dim != null){
                        images.push(scene_desktop_img_dim);                   
                }
            
            var scene_mobile_image_dim = scenes[i].scene_mobile_image_dim;
                if(scene_mobile_image_dim != "" && scene_mobile_image_dim != null){
                        images.push(scene_mobile_image_dim);                   
                }

            var scene_image_wof = scenes[i].scene_image_wof;
                if(scene_image_wof != "" && scene_image_wof != null){
                        images.push(scene_image_wof);                   
                }

        } // end for scenes

        for(var i=0; i<selfnav_floorplans.length; i++){
            var floorplan_image = selfnav_floorplans[i].floorplan_image;
                if(floorplan_image != "" && floorplan_image != null){
                    images.push(floorplan_image);                   
                }

            var floorplan_image_wof = selfnav_floorplans[i].floorplan_image_wof;
                if(floorplan_image_wof != "" && floorplan_image_wof != null){
                        images.push(floorplan_image_wof);                   
                }

        }
    
         
        //console.log(images);

        // setInterval(function(){
        //     $.imgpreload(images, function (){}); // end of image pre load
        // }, 2000); 

        var no_images_loaded = 0;
        var no_of_images = images.length; 

          $.imgpreload(images,
                    {

                         each:function(){
                            no_images_loaded++;
                        
                            var percentage = (no_images_loaded / no_of_images) * 100;
                            percentage = Math.round(percentage);
                        
                            $('#loader-perc').attr('data-pct',percentage);  

                            var degree = (no_images_loaded / no_of_images) * 360;
                            var pathElement = document.getElementById("circular-loader-exterior")
                            if (!pathElement) 
                            {
                                return; // don't do the rest
                            }
                            pathElement.setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, degree));
                            },
                         all:function ()
                        {
                           
                                $("#screen-loader-exterior").hide();
                                loadScene(data.scenes[0].scene_id, true);

                            //     <?php
                            // if ($view == "") {
                            //     ?>
                            //     loadScene(data.scenes[0].scene_id, true);
                                
                            // <?php } else { ?>
                            //     var viewindex = searchViewIndex(<?= $view ?>);
                            //     loadScene(data.scenes[viewindex].scene_id, true);
                            // <? } ?>
                          
                                
                        }
         });         
        
        
    }// end of cache_images function


