
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jordanhill Oak Phase</title>
</head>
<body>
<?php
//Note * In this code scene no is passed in id, becuase scene_no is passed from homeselector. so in loadscene we are fetching data using scene_no. In $postid we are passing self navigation id.

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$view = "";

if (isset($_GET['view'])) {
    $view = $_GET['view'];
}

$postid = $_GET['id']; //pass selfnavid
?>
<link rel="stylesheet" href="css/exterior.css" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" href="css/custom.css" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" href="css/style.css" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" href="css/loader.css" type="text/css" rel="stylesheet"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="js/loader.class.js"></script>

<div id="screen-loader-exterior" style="position: absolute;top:0;left:0;width:100vw;height:100vh;background-color: #fff;z-index: 2000;">
    <div class="loader-outer">
      
        <div  id="loader-perc" class="loader-perc" data-pct="0" style="display: none;"><sup style="font-size:36px;">%</sup></div>
     
        <svg width="500" height="500">
        <path id="circular-gray-exterior" fill="none" stroke="#ebebeb" stroke-width="49" />
            <path id="circular-ring-exterior" fill="none" stroke="#dac4b3" stroke-width="39" />
            <path id="circular-loader-exterior" fill="none" stroke="#87674f" stroke-width="39" />
        
        </svg>
   </div><!-- .loader-outer -->
     
</div><!-- #screen-loader -->
<script type="text/javascript" src="js/three.min.js"></script>
<script type='text/javascript' src='js/jquery.imgpreload.js'></script>

<style>.active_info{
        background-image: url("images/info_active.png"); 

    }
</style>
<script type="text/javascript">

//initiate as false
var isMobile = false;
// device detection
    // if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    //         || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))
        isMobile = true;

    function searchViewIndex(view) {
        view = parseInt(view);
        for (var i = 0; i < data.scenes.length; i++) {
            var number = parseInt(data.scenes[i].scene_no)
            if (number == view) {
                return i;
            }
        }
    } // end funcction searchViewIndex

    $(document).ready(function (e) {

        if (!isMobile) {
            $("#gyro").hide();
        }

 var degree = 0;
document.getElementById("circular-gray-exterior").setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, 359.99));
document.getElementById("circular-ring-exterior").setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, 359.99));
$('.loader-perc').css('display','block');


$.ajax({
    url: 'https://cala.thehomeselector.co.uk/wp/oak-phase/wp-json/api/v1/exterior-360-data',
    
    method: 'post',
    
    data: { selfnav_id: <?=$postid?> } ,
    
    success: function( data, textStatus, jQxhr ){
       // console.log(data );
        window.data = data;
        init_exterior(window.data);
      //  cache_images(data);
        angular.element($("#floorplan-container")).scope().updateExteriorData(window.data);
        angular.element($("#floorplan-container")).scope().$apply();
    },
    error: function( jqXhr, textStatus, errorThrown ){
        console.log( errorThrown );
    }
}); // end ajax call

    
                          
    });//document ready close

    function init_exterior(data)
    {
        console.log(data);
                    var j;
                    var k;
                    var images = [];
                    var len = data.scenes.length;
                    for (j = 0; j < len; j++) {
                        images.push(data.scenes[j].scene_image);

                    }
                    var no_images_loaded = 0;
                    var no_of_images = images.length;

                    $.imgpreload(images,
                    {

                         each:function(){
                            no_images_loaded++;
                        
                            var percentage = (no_images_loaded / no_of_images) * 100;
                            percentage = Math.round(percentage);
                        
                            $('#loader-perc').attr('data-pct',percentage);  

                            var degree = (no_images_loaded / no_of_images) * 360;
                            
                            document.getElementById("circular-loader-exterior").setAttribute("d", Loader.circular_loader(250, 250, 95.5, 0, degree));
                            },
                         all:function ()
                        {
                           
                               
                                <?php
                            if ($view == "") {
                                ?>
                                //loadScene(data.scenes[0].scene_id, true);
                              
                                setTimeout(function(){
                                      loadScene(data.scenes[0].scene_no, true);
                            }, 2000);
                            <?php } else { ?>
                                console.log("inside..");
                                var viewindex = searchViewIndex(<?= $view ?>);
                               
                                loadScene(data.scenes[viewindex].scene_no, true);
                           
                                
                            <? } ?>
                          
                          
                                    $("#screen-loader-exterior").hide();
                              
                        }
                        
         });
    }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.7/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.7/angular-sanitize.js"></script>
<script src="js/exterior.js" type="text/javascript"></script>
<script type="text/javascript">

console.log("Angular loaded");
var app = angular.module('appExtKey', ['ngSanitize']);

 app.controller('ctrlExtKey', function ($scope, $http) {
   // console.log("Browser Online Status: " + navigator.onLine);
    $scope.data = " ";
    $scope.updateExteriorData = function(data){
        $scope.data = data;
        //console.log('here');
      // console.log($scope.data);
      //init_selector(data);
        $scope.selfnav = $scope.data.selfnav;
        $scope.floorplans = $scope.selfnav.floorplans;
        $scope.floorid =  $scope.floorplans[0].floor;
        $scope.arrfloor = ['Ground','First','Second','Third','Fourth'];
        $scope.floorplan_img = $scope.floorplans[0].floorplan_image;
         //  console.log($scope.floorplan_img);
    }
   
       $scope.togglefloorplans = function(element) {
                
               
                //$('.btn-floor').removeClass('active');
               // $('.plan_'+element).addClass('active');
               
                if(window.activeFloor == 'plan_'+element){
                    
                    $('#people').css('display','block');
                }else{
                    $('#people').css('display','none');
                }

                $scope.floorid = element;
             
            }
      

}); 
// app.config(['$compileProvider', function ($compileProvider) {
//   $compileProvider.debugInfoEnabled(false);
// }]);
jQuery(document).ready(function($) 
{
    setTimeout(function(){
            //  loadScene(data.scenes[0].scene_no, true);
    }, 2000);
});
</script>

<!-- <div class="ext-logo" >  
         <img class="img-responsive " src="./images/jordan-hill-logo.png"> 
</div> -->
<!-- .logo -->

<div id="labels" style="position:absolute;left:0px;top:0px;">

</div>

<!--        <script type="text/javascript">

            var fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
            var fullscreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled;
            function launchIntoFullscreen(element) {
                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }

                var lock = window.navigator.requestWakeLock('screen');
            }

// Launch fullscreen for browsers that support it!
            addEventListener("touchend", function () {
                launchIntoFullscreen(document.documentElement); // the whole page
            });
        </script>-->

<!-- .Floorplans Container starts -->

 <div id="floorplan-container" ng-app="appExtKey" ng-controller="ctrlExtKey" >

 <div class="floorplan-inner" >
 
   <div class="floor"  ng-repeat="floorplan in floorplans" ng-show="floorplan.floor == floorid" id="floorid_{{floorplan.floor}}" >

       <div ng-repeat="tag in floorplan.tags" data-floor="{{floorplan.floor}}" class="btn_click btn-active-{{tag.scene_id}}" id="btn-{{tag.scene_no}}" data-sceneid="{{tag.scene_no}}" title="{{tag.scene_label}}" 
        style="background-image:url(./images/icon-location-scene.png); left:{{tag.coord_x}}px;top:{{tag.coord_y}}px;"
         target="{{tag.scene_id}}" onclick="loadScene(this.getAttribute('data-sceneid'), true)" >
         <div class="scene-label">{{tag.scene_no}}</div> 
         </div>
       
      <img id="floorplan_{{floorplan.floor}}" class="floor-img floor-wf"  ng-src=" {{floorplan.floorplan_image}}" style="display: block;" />
      <img id="floorplan_{{floorplan.floor}}" class="floor-img floor-wof"  ng-src="{{floorplan.floorplan_image_wof}}" style="display: none;"/>

       <div class="camera" id="people" style="display:none;z-index:101;position:absolute;">
        <div style="position:relative;">
            <img alt="0"  src="./images/camera.png" style="position:absolute;top:-41px;left:-41px;pointer-events: none;"/>
            
        </div>
    </div><!--#people -->

    </div>
    
 </div>

/*  <div class="floor-buttons" >
     <div class="div-centering">
         
         <!-- <div class="btn-floor plan_{{fl.floor}}  floor-clicks"   id="button"  ng-repeat="fl in floorplans" ng-click="togglefloorplans($index)"  ng-class="{active: $index== floorid}" target="{{arrfloor[fl.floor]}}" ng-style="{ 'width' : 'calc(209px/{{floorplans.length}})'}">{{arrfloor[fl.floor]}}</div> -->
         <!-- <div class="btn-floor plan_{{fl.floor}}  floor-clicks"   id="button"  ng-repeat="fl in floorplans" ng-click="togglefloorplans($index)"  ng-class="{active: $index== floorid}" target="{{arrfloor[fl.floor]}}">{{arrfloor[fl.floor]}}</div> -->

         </div>
         
     </div> */

</div>
</div>

<div id="overlay" style="position: absolute; top: 0px; left: 0px; width: 100vw; height: 100vh; background-color: rgb(51, 51, 51); z-index: 1000;display:none">
    <div class="overlay_inner">
        <div class="thecube">
            <div class="cube c1"></div>
            <div class="cube c2"></div>
            <div class="cube c4"></div>
            <div class="cube c3"></div>
        </div>
        <h4>  Please wait while your virtual tour loads.... </h4>
    </div>
</div> 

<ul style="position:absolute;top:10px;left:10px;">
<?php

//Temporary code 
// Remove once viewpoint map implemented
//for($c=0; $c<2; $c++){
    ?>
    <!-- <li style="width:60px;display:inline;cursor:pointer;color:#fff;" onclick="loadScene(data.scenes[<?=$c?>].scene_no, true);">VIEW <?=$c+1?> | </li> -->

<?php
//}

// end of termporary code
?>

</ul>
<div class="gyro-display">
    <img id="gyro" src="images/GSensor_Inactive.png" onclick="gyroOnOff();"/>  
</div>

</body>
</html>